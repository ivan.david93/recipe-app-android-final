package recipe.android.com.recipeappfinal.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.activity.ActivityThree;
import recipe.android.com.recipeappfinal.activity.ActivityTwo;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.CategoryB;
import recipe.android.com.recipeappfinal.model.Ingredient;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 05/08/2017.
 */

public class TabByIngredientsFragment extends Fragment implements ActivityThree.DataFromActivityToFragment {

    private static final String TAG = "Tab3Fragment";

    private static final String RECIPES_CACHE_FOLDER = "recipesFolder";

    private Button btnTEST;

    private static TextSwitcher textSwitcher;

//    private Button button;

    private List<CategoryB> lisOfAllCategoriesB;

    View view;

    private int size;
    private int index;

    private ArrayList<String> text;

    private ImageButton buttonLeft;
    private ImageButton buttonRight;

    private DataFromFragmentToFragment dataFromFragmentToFragment;

    private TextView badge_notifi_text;

    private Button butn_badge;

    private Button butn_generate;

    private List<Ingredient> arrayChoosenIngredients;
    private List<Recipe> listRecipesResult;

    private File currentRecipesDirFolder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_byingredients_layout, container, false);


//        text = new String[]{"Frescos", "Lacticinios", "Congelados", "Charcutaria"};
        text = new ArrayList<String>();
        text.add("NO DATA");
        size = text.size();

        index = -1;

        //        btnTEST = (Button) view.findViewById(R.id.btnTest);
       /* btnTEST.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Toast.makeText(getActivity(), "TESTING BUTTON CLICK 3 ", Toast.LENGTH_LONG).show();
            }
        });*/

        runSwitcherDef();

        //Hanfle the Child Fragment
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        IngredientListFragment ingredientListFragment = new IngredientListFragment();
        dataFromFragmentToFragment = (DataFromFragmentToFragment) ingredientListFragment;

        fragmentTransaction.replace(R.id.categoryFragment, ingredientListFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        //end


        butn_badge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showIngredientSelectList(arrayChoosenIngredients);
            }
        });


        return view;


    }


    private void runSwitcherDef() {
        init();
        setFactory();
        setListener();
        loadAnimations();

    }

    @Override
    public void sendData(String data) {
        //TODO fazer metodo
    }

    @Override
    public void sendDataList(List<CategoryB> data) {


        if (data != null && !data.isEmpty()) {
            lisOfAllCategoriesB = data;
            dataFromFragmentToFragment.sendDataList(lisOfAllCategoriesB);

        }


        for (int i = 0; i < lisOfAllCategoriesB.size(); i++) {

            CategoryB category = lisOfAllCategoriesB.get(i);
            String nameCat = category.getNameCategoryIngredientB();
            text.add(nameCat);
            size = text.size();
            //runSwitcherDef();

        }
    }


    void init() {
        //TODO BUTTON SWITCH TEST
//        button = (Button) view.findViewById(R.id.button_switcher);

        buttonLeft = (ImageButton) view.findViewById(R.id.button_switch_left);
        buttonRight = (ImageButton) view.findViewById(R.id.button_switch_right);

        textSwitcher = (TextSwitcher) view.findViewById(R.id.textSwitcher);

        badge_notifi_text = (TextView) view.findViewById(R.id.badge_notification_text);

        butn_badge = (Button) view.findViewById(R.id.button_badge);

        butn_generate = (Button) view.findViewById(R.id.generate_button);

        arrayChoosenIngredients = new ArrayList<>();
    }


    void loadAnimations() {

        Animation in = AnimationUtils.loadAnimation(this.getContext(), android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this.getContext(), android.R.anim.slide_out_right);
        textSwitcher.setInAnimation(in);
        textSwitcher.setOutAnimation(out);

        textSwitcher.setText(text.get(0));

    }


    void setFactory() {

        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            @Override
            public View makeView() {
                TextView myText = new TextView(getActivity());
                myText.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                myText.setTextSize(25);
                myText.setTextColor(Color.BLACK);
                return myText;
            }
        });

    }

    void setListener() {
        //TODO button switch test
       /* button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                if (index == size) {
                    index = 0;
                    textSwitcher.setText(text.get(index));
                }
                textSwitcher.setText(text.get(index));
            }
        });*/

        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index--;
                if (index <= 0) {
                    index = text.size() - 1;
                    textSwitcher.setText(text.get(index));
                    dataFromFragmentToFragment.sendData(text.get(index));
                    return;
                }
                textSwitcher.setText(text.get(index));
                dataFromFragmentToFragment.sendData(text.get(index));
            }
        });


        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                if (index == size) {
                    index = 0;
                    textSwitcher.setText(text.get(index));
                    dataFromFragmentToFragment.sendData(text.get(index));
                    return;
                }
                textSwitcher.setText(text.get(index));
                dataFromFragmentToFragment.sendData(text.get(index));
            }
        });

        butn_generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateRecipeByIngredients();
            }
        });

    }

    private void generateRecipeByIngredients() {
        
        //GET CURRENT USER ID 
        Integer userId = Integer.parseInt(HelperUtils.getUserDataFirebase().getUserId());
        List<Integer> newIngredientIdList = new ArrayList<>();
        for (Ingredient ingredient :arrayChoosenIngredients) {
            newIngredientIdList.add(ingredient.getIdIngredient());
        }
        getRecipesByIngredients(newIngredientIdList, userId);
        //getRecipesByIngredients(newIngredientIdList, null);
    }


    private void getRecipesByIngredients(List<Integer> ingredientsChoosenList,Integer idUser) {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);

        Call<List<Recipe>> call = taskService.getRecipesByIngredients(ingredientsChoosenList,idUser);
        call.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {

                listRecipesResult = response.body();

                if (listRecipesResult !=null && !listRecipesResult.isEmpty()) {
                    writeRecipes(listRecipesResult);
                    Log.d(TAG, "onResponse listRecipesResult : " + listRecipesResult);
                }
                if (listRecipesResult.isEmpty()) {
                    HelperUtils.toastMessage(getContext(),"No recipes found for selected ingredients!");
                }
            }

            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(TAG, "Error getRecipesByIngredients");
            }
        });

    }

    public void setBadgeValue(ArrayList<Ingredient> ingredientChoosenListSize) {
        badge_notifi_text.setText(Integer.toString(ingredientChoosenListSize.size()));
        arrayChoosenIngredients = ingredientChoosenListSize;

    }

    public interface DataFromFragmentToFragment {

        void sendData(String data);

        void sendDataList(List<CategoryB> lisOfAllCategoriesB);
    }


    private void showIngredientSelectList(List<Ingredient> arrayChoosenIngredients) {


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setIcon(R.drawable.ic_event_note_black);
        builderSingle.setTitle("Choosen ingredients: ");


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_singlechoice);
        if (arrayChoosenIngredients != null && !arrayChoosenIngredients.isEmpty()) {
            for (Ingredient ingredient : arrayChoosenIngredients) {
                arrayAdapter.add(ingredient.getIngredientName());
            }
//            arrayAdapter.add("Hardik");
//            arrayAdapter.add("Archit");
//            arrayAdapter.add("Jignesh");
//            arrayAdapter.add("Umang");
//            arrayAdapter.add("Gatti");
        } else {
            arrayAdapter.add("No ingredients selected!");
        }

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strName = arrayAdapter.getItem(which);
                AlertDialog.Builder builderInner = new AlertDialog.Builder(getActivity());
                builderInner.setMessage(strName);
                builderInner.setTitle("Your Selected Item is");
                builderInner.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builderInner.show();
            }
        });
        builderSingle.show();
    }




    private void writeRecipes(List<Recipe> lisOfRecipesByNum) {


        File cacheDir = getActivity().getBaseContext().getCacheDir();
        //Create a File for your desired directory

        currentRecipesDirFolder = new File(cacheDir,RECIPES_CACHE_FOLDER + "_"+ HelperUtils.getUserDataFirebase().getUserId() );

        //Call mkdirs() on that File to create the directory if it does not exist
        if (!currentRecipesDirFolder.exists()) {
            //directory is created;
            if (currentRecipesDirFolder.mkdir()) ;
        }

        //currentRecipesDirFolder = new File("/data/data/" + getContext().getPackageName() + "/files");
        int numOfStorageRecipes = currentRecipesDirFolder.listFiles().length;

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        String strDate = sdf.format(cal.getTime());

        if (numOfStorageRecipes < 5) {
            try {
                writeRecipesFromInternalMemory(strDate, lisOfRecipesByNum);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {


            File[] files = currentRecipesDirFolder.listFiles();

            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });

            files[0].delete();

            try {
                writeRecipesFromInternalMemory(strDate, lisOfRecipesByNum);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //after recipes generated and saved to storage go to activity two
        Intent listMealsIntent = new Intent(getContext(), ActivityTwo.class);
        getActivity().startActivity(listMealsIntent);


    }

    private void writeRecipesFromInternalMemory(String params, List<Recipe> recipes) throws IOException {


        String FILE_NAME = "recipeList_storage_date_" + params;

        File cacheDir = getActivity().getBaseContext().getCacheDir();
        //Create a File for your desired directory
        File direct = new File(cacheDir, RECIPES_CACHE_FOLDER + "_"+ HelperUtils.getUserDataFirebase().getUserId());
        //Call mkdirs() on that File to create the directory if it does not exist
        if (!direct.exists()) {
            if (direct.mkdir()) ; //directory is created;
        }
        //Create a File for the output file
        File file = new File(direct, FILE_NAME);

        FileOutputStream outStream;

        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //commented line throws an exception if filename contains a path separator
            //outstream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outStream = new FileOutputStream(file);
//            outstream.write(message.getBytes());
//            outstream.close();
            ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
            objectOutStream.writeInt(recipes.size()); // Save size first
            for (Recipe recipe : recipes) {
                objectOutStream.writeObject(recipe);
            }
            objectOutStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        /*FileOutputStream outStream = new FileOutputStream("/data/data/" + getContext().getPackageName() + "/files/Storage_Date_" + params);
        ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
        objectOutStream.writeInt(recipes.size()); // Save size first
        for (Recipe recipe : recipes)
            objectOutStream.writeObject(recipe);
        objectOutStream.close();
*/
        //readFromCache(FILE_NAME);

    }


}
