package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Recipe implements Serializable{

    @SerializedName("idRecipe")
    @Expose
    private Integer idRecipe;

    @SerializedName("rating")
    @Expose
    private Integer rating;

    @SerializedName("media")
    @Expose
    private String media;

    @SerializedName("difficulty")
    @Expose
    private Integer difficulty;

    @SerializedName("prepTime")
    @Expose
    private Integer prepTime;

    @SerializedName("nameRecipe")
    @Expose
    private String nameRecipe;

    @SerializedName("directions")
    @Expose
    private String directions;

    @SerializedName("ingredients")
    @Expose
    private List<RecipeIngredient> ingredients;

    @SerializedName("userCategoryList")
    @Expose
    private List<UserCategory> userCategoryList;

    @SerializedName("createDate")
    @Expose
    private Date createDate;

    @SerializedName("updateDate")
    @Expose
    private Date updateDate;

    @SerializedName("numberOfRatings")
    @Expose
    private Integer numberOfRatings;



    public Recipe() {
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "idRecipe=" + idRecipe +
                ", rating=" + rating +
                ", media='" + media + '\'' +
                ", difficulty=" + difficulty +
                ", prepTime=" + prepTime +
                ", nameRecipe='" + nameRecipe + '\'' +
                ", directions='" + directions + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", numberOfRatings=" + numberOfRatings +
                '}';
    }

    public Integer getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(Integer idRecipe) {
        this.idRecipe = idRecipe;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public Integer getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Integer difficulty) {
        this.difficulty = difficulty;
    }

    public Integer getPrepTime() {
        return prepTime;
    }

    public void setPrepTime(Integer prepTime) {
        this.prepTime = prepTime;
    }

    public String getNameRecipe() {
        return nameRecipe;
    }

    public void setNameRecipe(String nameRecipe) {
        this.nameRecipe = nameRecipe;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }



    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getNumberOfRatings() {
        return numberOfRatings;
    }

    public void setNumberOfRatings(Integer numberOfRatings) {
        this.numberOfRatings = numberOfRatings;
    }

    public List<RecipeIngredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<RecipeIngredient> ingredients) {
        this.ingredients = ingredients;
    }

    public List<UserCategory> getUserCategoryList() {
        return userCategoryList;
    }

    public void setUserCategoryList(List<UserCategory> userCategoryList) {
        this.userCategoryList = userCategoryList;
    }
}