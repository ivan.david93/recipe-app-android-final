package recipe.android.com.recipeappfinal.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.UserCategory;
import recipe.android.com.recipeappfinal.model.UserDataFirebase;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pedro on 18/08/2017.
 */

public class ProfileScreenXMLUIDesign extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "ProfileActivity";

    private ImageView backBtn;
    private TextView user_profile_name;//,userNameTV,addressTV,birthdayTV,genreTV,passwordTV;
    private EditText userNameET, addressET;
    private Button btnChooseCategories, btnUpdateProfile, passwordResetBtn;
    private RadioGroup genreRG;
    private String radioGenderChoose;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReferenceUsers;
    private FirebaseUser user;


    private List<UserCategory> callUserCategory;

    boolean[] checkedItems;
    ArrayList<Integer> mDietTypes;
    //private List<Integer> userCategoryList;


    private List<UserCategory> lisOfAllUserCategories;
    private List<Integer> lisOfUserCategoriesIdToUpdate;
    private String listDiets[];
    private int idUser;


    //HashMap<String, String> userProfileInfo = new HashMap<String, String>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lisOfAllUserCategories = new ArrayList<>();
        callUserCategory = new ArrayList<>();
        mDietTypes = new ArrayList<>();
        lisOfUserCategoriesIdToUpdate = new ArrayList<>();


        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.material_design_profile_screen_xml_ui_design, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);


        //user_profile_name = (TextView) findViewById(R.id.user_profile_name);
        //userProfileInfo.put("userGenre",userGenre);
        userNameET = (EditText) findViewById(R.id.idUserNameET);
        addressET = (EditText) findViewById(R.id.idAddressET);

        backBtn = (ImageView) findViewById(R.id.backBtn);
        passwordResetBtn = (Button) findViewById(R.id.passwordResetBtn);

        btnChooseCategories = (Button) findViewById(R.id.btnChooseCat);
        btnChooseCategories.setOnClickListener(this);
        btnUpdateProfile = (Button) findViewById(R.id.btnUpdate);
        btnUpdateProfile.setOnClickListener(this);
        passwordResetBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);

        genreRG = (RadioGroup) findViewById(R.id.radio_group);
        genreRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                switch (checkedId) {
                    case R.id.radioMale:
                        // do operations specific to this selection
                        radioGenderChoose = "male";
                        break;
                    case R.id.radioFemale:
                        // do operations specific to this selection
                        radioGenderChoose = "female";
                        break;
                    case R.id.radioNon:
                        // do operations specific to this selection
                        radioGenderChoose = "non-binary";
                        break;
                }

            }
        });

        getAllUserCategoriesBD();
        getUserProfileInfo();


        //listDiets = getResources().getStringArray(R.array.diet_types);
        //checkedItems = new boolean[listDiets.length];

    }

    @Override
    public void onClick(View view) {

        if (view ==backBtn) {

            Intent intent2 = new Intent(this, MainActivity.class);
            startActivity(intent2);
            finish();        }


        if (view == btnChooseCategories) {
            runDietTypeDialog();

        }

        if (view == passwordResetBtn) {
            goToResetPasswrd();

        }

        if (view == btnUpdateProfile) {
            updateUserToDatabase(idUser, lisOfUserCategoriesIdToUpdate);
            // Intent intent = new Intent(this, ProfileScreenXMLUIDesign.class);
            //startActivity(intent);
            //finish();
        }


    }

    private void goToResetPasswrd() {
        Intent intent = new Intent(this, ActivityForgetPassword.class);
        startActivity(intent);
        //finish();
    }

    private void runDietTypeDialog() {
        checkedItems = new boolean[listDiets.length];
        for (int o = 0; o < callUserCategory.size(); o++) {
            String userCategoryName = callUserCategory.get(o).getNameUserCategory().toString();
            for (int i = 0; i < checkedItems.length; i++) {

                if (listDiets[i].equalsIgnoreCase(userCategoryName)) {
                    checkedItems[i] = true;
                    mDietTypes.add(i);
                }
            }
        }


//        Log.d(TAG, "------------------------------: " + lisOfAllUserCategories.get(0).getNameUserCategory().toString());
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileScreenXMLUIDesign.this);
        builder.setTitle(R.string.str_diet_types);
        builder.setMultiChoiceItems(listDiets, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position, boolean isChecked) {

                if (isChecked) {
                    //verify if item is selected or not
                    if (!mDietTypes.contains(position)) {
                        mDietTypes.add(position);
                    }

                } else if (mDietTypes.contains(position)) {
                    // removes the object X - if pass only position  thinks that "position" is a index not a object
                    mDietTypes.remove(Integer.valueOf(position));
                }
            }
        });
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.str_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //TODO IF Ok guarda na database categorias de user diet type para este user
                for (int i = 0; i < mDietTypes.size(); i++) {
                    Log.d(TAG, "mDietTypes: " + listDiets[mDietTypes.get(i)]);
                    // adiciona os ids das categorias pretendidas pelo utilizador à lista
                    //userCategoryList.add(mDietTypes.get(i) + 1);
                }
                toastMessage("onClick ok dialog");

                //Criar lista de USer categories para update
                for (int p = 0; p < mDietTypes.size(); p++) {
                    for (int i = 0; i < lisOfAllUserCategories.size(); i++) {
                        String userCategNameInAllUC = lisOfAllUserCategories.get(i).getNameUserCategory().toString();
                        String userCategNameInSelectUC = listDiets[mDietTypes.get(p)];
                        if (userCategNameInAllUC.equalsIgnoreCase(userCategNameInSelectUC)) {
                            lisOfUserCategoriesIdToUpdate.add(lisOfAllUserCategories.get(i).getIdUserCategory());
                        }
                    }


                }
               /* for (UserCategory u:lisOfUserCategoriesIdToUpdate
                     ) {
                    Log.d(TAG, "As categorias para update do "+idUser+ "+ sao ----> " + u.getNameUserCategory().toString());
                }*/


                //toastMessage(userCategoryList.toString());

            }
        });


        builder.setNeutralButton(R.string.str_clear_all, new DialogInterface.OnClickListener()

        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < checkedItems.length; i++) {
                    checkedItems[i] = false;
                    mDietTypes.clear();
                }
                //Log.d(TAG, "------------------------ ---->" + mDietTypes.toString());
            }
        });

        AlertDialog mdialog = builder.create();

        mdialog.show();
    }


    private void updateUserToDatabase(int userId, List<Integer> userCategoryIdList) {
        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<Boolean> call = taskService.updateUserBD(userId, userCategoryIdList);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {


                Boolean callUpdateUserResponse = response.body();

                if (callUpdateUserResponse != null && callUpdateUserResponse) {

                    updateUserInFireBase();
                    toastMessage("Fez Update.....");
                }


            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {

            }
        });
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    private void getUserProfileInfo() {

        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference("users");
        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        // Get a reference to our posts
        //final FirebaseDatabase database = FirebaseDatabase.getInstance();
        //DatabaseReference ref = database.getReference("users");

// Attach a listener to read the data at our posts reference
        final String UUID = user.getUid();


        databaseReferenceUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //for (DataSnapshot dsChildren : dataSnapshot.getChildren()) {

                String userId = dataSnapshot.child(UUID).getValue(UserDataFirebase.class).getUserId();
                String userName = dataSnapshot.child(UUID).getValue(UserDataFirebase.class).getUserName();
                String userAddress = dataSnapshot.child(UUID).getValue(UserDataFirebase.class).getAddress();
                String userGenre = dataSnapshot.child(UUID).getValue(UserDataFirebase.class).getGender();

                int userIdInt = Integer.parseInt(userId);
                idUser = userIdInt;

                getUserCategoryByUserId(userIdInt);
                //METODO QUE FAZ SET DA INFO USER
                if (userId != null) {
                    setUserInfo(userId, userName, userAddress, userGenre);
                }
                //}
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });


    }

    private void setUserInfo(String userId, String userName, String userAddress, String userGenre) {


        //Log.d(TAG, "userId ---->" + userProfileInfo.get("userId"));
        //user_profile_name.setText(userName);
        userNameET.setText(userName);
        addressET.setText(userAddress);


        if (userGenre.equalsIgnoreCase("male")) {
            radioGenderChoose = "male";
            genreRG.check(R.id.radioMale);
        }
        if (userGenre.equalsIgnoreCase("female")) {
            radioGenderChoose = "female";
            genreRG.check(R.id.radioFemale);
        }
        if (userGenre.equalsIgnoreCase("Non-binary")) {
            radioGenderChoose = "non-binary";
            genreRG.check(R.id.radioNon);
        }


    }


    private void getAllUserCategoriesBD() {
        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<UserCategory>> call = taskService.getAllUserCategories();
        call.enqueue(new Callback<List<UserCategory>>() {
            @Override
            public void onResponse(Call<List<UserCategory>> call, Response<List<UserCategory>> response) {

                List<UserCategory> lisOfAllUserCategoriesResponse = response.body();
                if (lisOfAllUserCategoriesResponse != null && !lisOfAllUserCategoriesResponse.isEmpty()) {
                    lisOfAllUserCategories = lisOfAllUserCategoriesResponse;
                    listDiets = new String[lisOfAllUserCategories.size()];
                    for (int i = 0; i < lisOfAllUserCategories.size(); i++) {
                        listDiets[i] = lisOfAllUserCategories.get(i).getNameUserCategory().toString();
                    }
                }
                //Log.d(TAG, "As categorias existentes sao ---->" + lisOfAllUserCategories.get(0).getNameUserCategory());
            }

            @Override
            public void onFailure(Call<List<UserCategory>> call, Throwable t) {
            }
        });
    }

    private void getUserCategoryByUserId(int userId) {
        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<UserCategory>> call = taskService.getUserCategoryByUserId(userId);
        call.enqueue(new Callback<List<UserCategory>>() {
            @Override
            public void onResponse(Call<List<UserCategory>> call, Response<List<UserCategory>> response) {


                List<UserCategory> callUserCategoryResponse = response.body();

                if (callUserCategoryResponse != null && !callUserCategoryResponse.isEmpty()) {
                    callUserCategory = callUserCategoryResponse;

                }


            }

            @Override
            public void onFailure(Call<List<UserCategory>> call, Throwable t) {

            }
        });
    }

    private void updateUserInFireBase() {
        Log.d(TAG, "---------------------------- ENTROI .------------");
        //save user to firebase with update user
        DatabaseReference databaseReferenceUsers = HelperUtils.getDatabaseReferenceUsers();
        UserDataFirebase userDataFirebase = HelperUtils.getUserDataFirebase();
        FirebaseUser firebaseUser = HelperUtils.getFirebaseUser();

        userDataFirebase.setUserName(userNameET.getText().toString());
        userDataFirebase.setGender(radioGenderChoose);
        userDataFirebase.setAddress(addressET.getText().toString());
        databaseReferenceUsers.child(firebaseUser.getUid()).setValue(userDataFirebase);

    }
}
