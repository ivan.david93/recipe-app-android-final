package recipe.android.com.recipeappfinal.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import recipe.android.com.recipeappfinal.R;

/**
 * Created by IVAN on 05/08/2017.
 */

public class ActivityOne extends AppCompatActivity {

    /*
    *
    *
    *
    * TODO  ESTA ACTIVIDAD NAO FAZ NADA PARA JA
    *
    *
    *
    *
    * */

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        TextView title1 = (TextView) findViewById(R.id.activityTitle1);
        title1.setText("This is ACTIVITY HOME");


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        //TODO DISABLE SHIFT MODE ON BOTTOM IF WANTED HERE
        // BottomNavViewHelper.disableShiftMode(bottomNavigationView);

        // declarare items menu to highlighht each button
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
//                    case R.id.ic_home_black:
//                        Intent intent1 = new Intent(ActivityOne.this,ActivityOne.class);
//                        startActivity(intent1);
//                        break;
                    case R.id.ic_view_list:
                        Intent intent2 = new Intent(ActivityOne.this,ActivityTwo.class);
                        startActivity(intent2);
                        break;
                    case R.id.ic_shuffle_black:
                        Intent intent3 = new Intent(ActivityOne.this,ActivityThree.class);
                        startActivity(intent3);
                        break;

                }


                return false;

            }
        });
    }




}
