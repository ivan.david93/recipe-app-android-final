package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedro on 03/06/2017.
 */

public class UserCategory implements Serializable {

    @SerializedName("idUserCategory")
    @Expose
    private Integer idUserCategory;

    @SerializedName("nameUserCategory")
    @Expose
    private String nameUserCategory;

    @SerializedName("userList")
    @Expose
    public List<User> userList = new ArrayList<User>();

    @SerializedName("recipeList")
    @Expose
   public List<Recipe> recipeList = new ArrayList<Recipe>();

    public UserCategory() {
    }

    public Integer getIdUserCategory() {
        return idUserCategory;
    }

    public void setIdUserCategory(Integer idUserCategory) {
        this.idUserCategory = idUserCategory;
    }

    public String getNameUserCategory() {
        return nameUserCategory;
    }

    public void setNameUserCategory(String nameUserCategory) {
        this.nameUserCategory = nameUserCategory;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<Recipe> getRecipeList() {
        return recipeList;
    }

    public void setRecipeList(List<Recipe> recipeList) {
        this.recipeList = recipeList;
    }
}
