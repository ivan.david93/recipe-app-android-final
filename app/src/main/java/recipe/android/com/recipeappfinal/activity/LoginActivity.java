package recipe.android.com.recipeappfinal.activity;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.model.UserToLocal;
import recipe.android.com.recipeappfinal.utils.HelperUtils;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    private static final String USER_CACHE_FOLDER = "userFolder";

    private FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener;

    private EditText mEmail, mPassword;

    private Button btnSignIn;

    private ProgressDialog progressDialog;

    private TextView textNotRegistered;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog = new ProgressDialog(this);

        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        btnSignIn = (Button) findViewById(R.id.email_sign_in_button);
        textNotRegistered = (TextView) findViewById(R.id.textNotRegistered);

        mAuth = FirebaseAuth.getInstance();

        HelperUtils.checkIsServerAlive();


        //THIS DETECTS DE USER STATE CHANGED AND GO TO NEW ACTIVITY MAIN
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    //toastMessage("Successfully signed in with: " + user.getEmail());
                    //THIS IS A HELPER CLASS THAT GET DATA FROM FIREBASE AND SAVE USER TO BE USED AT EVERY CLASS AND CONTEXT
                    //HelperUtils.getCurrentUserDataFromFireBase(user);
                    //go to main activity
                    saveUserToLocalMemory(mEmail.getText().toString(), mPassword.getText().toString());
                    goToMain();


                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    //toastMessage("Successfully signed out");
                }
                // ...
            }


        };


        btnSignIn.setOnClickListener(this);


        textNotRegistered.setOnClickListener(this);

    }

    private void saveUserToLocalMemory(String email, String password) {

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        String strDate = sdf.format(cal.getTime());

        try {
            writeUserToInternalMemory(strDate, email, password);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        if (view == btnSignIn) {

            String email = mEmail.getText().toString();
            String pass = mPassword.getText().toString();

            if ((!email.equals("") || !email.trim().isEmpty()) && (!pass.equals("") || !pass.trim().isEmpty())) {

                progressDialog.setMessage("Login in...");
                progressDialog.show();

                mAuth.signInWithEmailAndPassword(email, pass)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {


                                if (task.isSuccessful()) {
                                    progressDialog.dismiss();
                                    toastMessage("Login success");
                                    Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                                }


                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    progressDialog.dismiss();
                                    toastMessage("Wrong user email ou password!");
                                    Log.w(TAG, "signInWithEmail", task.getException());
                                    try {
                                        throw task.getException();

                                    } catch (Exception e) {
                                        Log.e(TAG, e.getMessage());
                                    }

                                }
                            }
                        });


            } else {
                toastMessage("You didn´t fill in all the fields!");
                progressDialog.dismiss();
            }
        }


        if (view == textNotRegistered) {


            if (HelperUtils.isAlive()) {
                Intent intent = new Intent(this, RegisterActivity.class);
                startActivity(intent);
                finish();
            } else {
                HelperUtils.serverDownMessage(LoginActivity.this, "Can´t navigate to register!");
            }




        }


    }


    private void checkIfLoggedIn() {
        //if exists user logged in  then loggof
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            this.mAuth.signOut();
        }

    }

    private void goToMain() {
        progressDialog.dismiss();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    private void writeUserToInternalMemory(String params, String email, String password) throws IOException {

        UserToLocal userToLocal = new UserToLocal(email, password);


        String FILE_NAME = "user_storage_date_" + params;

        File cacheDir = this.getBaseContext().getCacheDir();
        //Create a File for your desired directory
        File direct = new File(cacheDir, USER_CACHE_FOLDER);
        //Call mkdirs() on that File to create the directory if it does not exist
        if (!direct.exists()) {
            if (direct.mkdir()) ; //directory is created;
        }
        //Create a File for the output file
        File file = new File(direct, FILE_NAME);

        FileOutputStream outStream;

        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //commented line throws an exception if filename contains a path separator
            //outstream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outStream = new FileOutputStream(file);
            ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
            objectOutStream.writeObject(userToLocal);
            objectOutStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        /*FileOutputStream outStream = new FileOutputStream("/data/data/" + getContext().getPackageName() + "/files/Storage_Date_" + params);
        ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
        objectOutStream.writeInt(recipes.size()); // Save size first
        for (Recipe recipe : recipes)
            objectOutStream.writeObject(recipe);
        objectOutStream.close();
*/
        try {
            readUserFromInternalMemory();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    private UserToLocal readUserFromInternalMemory() throws IOException, ClassNotFoundException {

        File cacheDir = getBaseContext().getCacheDir();
        //Create a File for your desired directory
        File currentUserDirFolder = new File(cacheDir, USER_CACHE_FOLDER);

        //Call mkdirs() on that File to create the directory if it does not exist
        //Call mkdirs() on that File to create the directory if it does not exist
        if (!currentUserDirFolder.exists()) {
            //directory is created;
            currentUserDirFolder.mkdir();
        }


        File[] files = currentUserDirFolder.listFiles();


        Arrays.sort(files, new Comparator<File>() {
            public int compare(File f1, File f2) {
                return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
            }
        });

        File fileSelected = files[files.length - 1];
        String absolutePath = fileSelected.getAbsolutePath();


        //FileInputStream inStream = new FileInputStream("/data/data/" + getApplicationContext().getPackageName() + "/files/" + params);
        FileInputStream inStream = new FileInputStream(absolutePath);

        ObjectInputStream objectInStream = new ObjectInputStream(inStream);
        UserToLocal userToLocal = (UserToLocal) objectInStream.readObject();
        objectInStream.close();


        return userToLocal;
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


}

