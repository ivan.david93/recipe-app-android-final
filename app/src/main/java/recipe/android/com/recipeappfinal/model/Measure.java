package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedro on 03/06/2017.
 */

public class Measure implements Serializable {
    @SerializedName("idMeasure")
    @Expose
    private int idMeasure;

    @SerializedName("nameMeasure")
    @Expose
    private String nameMeasure;

    @SerializedName("initialsMeasure")
    @Expose
    private String initialsMeasure;

    @SerializedName("ingredients")
    @Expose
    private List<Ingredient> ingredients = new ArrayList<Ingredient>();


    public Measure() {
    }

    public Measure(int idMeasure, String nameMeasure, String initialsMeasure) {
        this.idMeasure = idMeasure;
        this.nameMeasure = nameMeasure;
        this.initialsMeasure = initialsMeasure;
    }

    public int getIdMeasure() {
        return idMeasure;
    }

    public void setIdMeasure(int idMeasure) {
        this.idMeasure = idMeasure;
    }

    public String getNameMeasure() {
        return nameMeasure;
    }

    public void setNameMeasure(String nameMeasure) {
        this.nameMeasure = nameMeasure;
    }

    public String getInitialsMeasure() {
        return initialsMeasure;
    }

    public void setInitialsMeasure(String initialsMeasure) {
        this.initialsMeasure = initialsMeasure;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
