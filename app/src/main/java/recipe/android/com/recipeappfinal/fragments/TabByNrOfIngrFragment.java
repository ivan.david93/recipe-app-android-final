package recipe.android.com.recipeappfinal.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.activity.ActivityTwo;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 05/08/2017.
 */

public class TabByNrOfIngrFragment extends Fragment {

    private static final String TAG = "Tab3Fragment";
    private static final String RECIPES_CACHE_FOLDER = "recipesFolder";
    File currentRecipesDirFolder;

    private Button btnTEST;
    NumberPicker numPicker = null;
    List<Recipe> lisOfRecipesByNum;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_bynr_of_ingr_layout, container, false);
        btnTEST = (Button) view.findViewById(R.id.btnTest);

        numPicker = (NumberPicker) view.findViewById(R.id.idNumberPicker);
        numPicker.setMaxValue(10);
        numPicker.setMinValue(1);
        numPicker.setWrapSelectorWheel(false);
        lisOfRecipesByNum = new ArrayList<Recipe>();


        btnTEST.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Toast.makeText(getActivity(), "TESTING BUTTON CLICK 3 --> " + numPicker.getValue(), Toast.LENGTH_LONG).show();
                getRecipesByNum();
            }
        });
        return view;


    }

    private void getRecipesByNum() {

        List<Integer> idListCategory = new ArrayList<Integer>();
        idListCategory.add(1);
        idListCategory.add(2);


        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<Recipe>> call = taskService.getRecipesByNumOfIng(idListCategory, numPicker.getValue());
        //Call<List<Recipe>> call = taskService.getRecipesByNum(numPicker.getValue());
        call.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {

                lisOfRecipesByNum = response.body();

                if (lisOfRecipesByNum != null && !lisOfRecipesByNum.isEmpty()) {
                    writeRecipes(lisOfRecipesByNum);
                }
            }
            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {

            }
        });

    }

    private void writeRecipes(List<Recipe> lisOfRecipesByNum) {


        File cacheDir = getActivity().getBaseContext().getCacheDir();
        //Create a File for your desired directory

        currentRecipesDirFolder = new File(cacheDir,RECIPES_CACHE_FOLDER + "_"+ HelperUtils.getUserDataFirebase().getUserId() );

        //Call mkdirs() on that File to create the directory if it does not exist
        if (!currentRecipesDirFolder.exists()) {
            //directory is created;
            if (currentRecipesDirFolder.mkdir()) ;
        }

        //currentRecipesDirFolder = new File("/data/data/" + getContext().getPackageName() + "/files");
        int numOfStorageRecipes = currentRecipesDirFolder.listFiles().length;

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        String strDate = sdf.format(cal.getTime());
        if (numOfStorageRecipes < 5) {
            try {
                writeRecipesFromInternalMemory(strDate, lisOfRecipesByNum);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {


            File[] files = currentRecipesDirFolder.listFiles();

            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });

            files[0].delete();

            try {
                writeRecipesFromInternalMemory(strDate, lisOfRecipesByNum);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //after recipes generated and saved to storage go to activity two
        Intent listMealsIntent = new Intent(getContext(), ActivityTwo.class);
        getActivity().startActivity(listMealsIntent);


    }

    private void writeRecipesFromInternalMemory(String params, List<Recipe> recipes) throws IOException {


        String FILE_NAME = "recipeList_storage_date_" + params;

        File cacheDir = getActivity().getBaseContext().getCacheDir();
        //Create a File for your desired directory
        File direct = new File(cacheDir, RECIPES_CACHE_FOLDER + "_"+ HelperUtils.getUserDataFirebase().getUserId());
        //Call mkdirs() on that File to create the directory if it does not exist
        if (!direct.exists()) {
            if (direct.mkdir()) ; //directory is created;
        }
        //Create a File for the output file
        File file = new File(direct, FILE_NAME);

        FileOutputStream outStream;

        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //commented line throws an exception if filename contains a path separator
            //outstream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outStream = new FileOutputStream(file);
//            outstream.write(message.getBytes());
//            outstream.close();
            ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
            objectOutStream.writeInt(recipes.size()); // Save size first
            for (Recipe recipe : recipes) {
                objectOutStream.writeObject(recipe);
            }
            objectOutStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        /*FileOutputStream outStream = new FileOutputStream("/data/data/" + getContext().getPackageName() + "/files/Storage_Date_" + params);
        ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
        objectOutStream.writeInt(recipes.size()); // Save size first
        for (Recipe recipe : recipes)
            objectOutStream.writeObject(recipe);
        objectOutStream.close();
*/
        //readFromCache(FILE_NAME);

    }



    public List<Recipe> readFromCache(String FILE_NAME) {

       /* try {
            String message;
            FileInputStream fileInputStream = openFileInput("testFile");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ((message = bufferedReader.readLine()) != null) {
                stringBuffer.append(message + "\n");
                Log.d(TAG,message);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        File cacheDir = getActivity().getBaseContext().getCacheDir();
        //Create a File for your desired directory
        File direct = new File(cacheDir, RECIPES_CACHE_FOLDER + "_"+ HelperUtils.getUserDataFirebase().getUserId());

        File file = new File(direct+"/" + FILE_NAME) ;


        FileInputStream inStream = null;
        try {
            inStream = new FileInputStream(file.getPath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        ObjectInputStream objectInStream = null;
        try {
            objectInStream = new ObjectInputStream(inStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int count = 0; // Get the number of regions
        try {
            count = objectInStream.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }


        List<Recipe> listOfStoragedRecipe = new ArrayList<Recipe>();

        for (int c = 0; c < count; c++)
            try {
                listOfStoragedRecipe.add((Recipe) objectInStream.readObject());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        try {
            objectInStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "=============== LISTA FINALLL ==> " + listOfStoragedRecipe.get(0));
        return listOfStoragedRecipe;

    }



}
