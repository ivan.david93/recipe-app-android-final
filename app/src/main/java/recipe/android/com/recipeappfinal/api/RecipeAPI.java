package recipe.android.com.recipeappfinal.api;

import java.util.List;

import recipe.android.com.recipeappfinal.model.CategoryA;
import recipe.android.com.recipeappfinal.model.CategoryB;
import recipe.android.com.recipeappfinal.model.IngredientHelper;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.model.User;
import recipe.android.com.recipeappfinal.model.UserCategory;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by IVAN on 08/08/2017.
 */

public interface RecipeAPI {


    @POST("recipe/findAll")
    Call<List<Recipe>> getRecipes();

    //@POST("findRecipesByNumOfIng")
    //Call<List<Recipe>> getRecipesByNumOfIng();
    
    @GET("ingredient/findAllCategoriesA")
    Call<List<CategoryA>> getCategoriesA();


    @GET("ingredient/findAllCategoriesB")
    Call<List<CategoryB>> getCategoriesB();

    @FormUrlEncoded
    @POST("ingredient/findRecipesByIngredients")
    Call<List<Recipe>> getRecipesByIngredients(@Field("ingredientsChoosenList") List<Integer> ingredientsChoosenList,@Field("idUser")  Integer idUser);
    
    @FormUrlEncoded
    @POST("recipe/findRecipesByNumOfIng")
    Call<List<Recipe>> getRecipesByNumOfIng(@Field("idListCategory") List<Integer> idListCategory,@Field("numOfRecipes") int numOfRecipes);

    @FormUrlEncoded
    @POST("recipe/findListRecipesByIds")
    Call<List<Recipe>> findListRecipesByIds(@Field("idListRecipes") List<Integer> idListRecipes);

    @FormUrlEncoded
    @POST("recipe/findRecipeById")
    Call<Recipe> getRecipeById(@Field("idRecipe") int idRecipe);

    @FormUrlEncoded
    @POST("ingredient/findIngredientsByRecipe")
    Call<List<IngredientHelper>> getIngredientsFromRecipe(@Field("idRecipe") int idRecipe);

    @FormUrlEncoded
    @POST("recipe/rateRecipe")
    Call<Recipe> rateRecipe(@Field("rating") float rating,@Field("idRecipe") int idRecipe);

    @GET("recipe/isServerAlive")
    Call<Boolean> isServerAlive();

    @FormUrlEncoded
    @POST("user/addUsertoBD")
    Call<User> addUsertoBD(@Field("userCategoryList") List<Integer> userCategoryList);

    @FormUrlEncoded
    @POST("ingredient/findIngredientstTotalByRecipesIds")
    Call<List<IngredientHelper>> getIngredientsFromRecipes(@Field("listIdRecipes")List<Integer> listIdRecipes);

    @POST("userCategory/findAll")
    Call<List<UserCategory>> getAllUserCategories();

    @FormUrlEncoded
    @POST("userCategory/findUserCategoryByUserId")
    Call<List<UserCategory>> getUserCategoryByUserId(@Field("userId") int userId);

    @FormUrlEncoded
    @POST("user/findUserById")
    Call<User> getUserById(@Field("userId") int userId);

    @FormUrlEncoded
    @POST("user/updateUser")
    Call<Boolean> updateUserBD(@Field("idUser") int idUser,@Field("userCategoryIdList") List<Integer> userCategoryIdList);
}
