package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedro on 03/06/2017.
 */

public class CategoryA implements Serializable {

    @SerializedName("idCategoryIngredientA")
    @Expose
   private int idCategoryIngredientA;

    @SerializedName("nameCategoryIngredientA")
    @Expose
   private String nameCategoryIngredientA;

    @SerializedName("listCategoriesB")
    @Expose
   private List<CategoryB> listCategoriesB = new ArrayList<CategoryB>();

    public CategoryA() {
    }

    public int getIdCategoryIngredientA() {
        return idCategoryIngredientA;
    }

    public void setIdCategoryIngredientA(int idCategoryIngredientA) {
        this.idCategoryIngredientA = idCategoryIngredientA;
    }

    public String getNameCategoryIngredientA() {
        return nameCategoryIngredientA;
    }

    public void setNameCategoryIngredientA(String nameCategoryIngredientA) {
        this.nameCategoryIngredientA = nameCategoryIngredientA;
    }

    public List<CategoryB> getListCategoriesB() {
        return listCategoriesB;
    }

    public void setListCategoriesB(List<CategoryB> listCategoriesB) {
        this.listCategoriesB = listCategoriesB;
    }

    @Override
    public String toString() {
        return "CategoryA{" +
                "idCategoryIngredientA=" + idCategoryIngredientA +
                ", nameCategoryIngredientA='" + nameCategoryIngredientA + '\'' +
                ", listCategoriesB=" + listCategoriesB +
                '}';
    }
}
