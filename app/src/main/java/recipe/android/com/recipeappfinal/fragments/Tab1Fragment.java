package recipe.android.com.recipeappfinal.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.FileOutputStream;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.activity.LoginActivity;
import recipe.android.com.recipeappfinal.activity.ShopListActivity;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by IVAN on 05/08/2017.
 */

public class Tab1Fragment extends Fragment {

    private static final String TAG = "Tab1Fragment";

    private Button btnTEST;
    private Button btnSignOut;

    private FirebaseAuth mAuth;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment1_layout,container,false);

        //TEST BUTTONS
        btnTEST = (Button) view.findViewById(R.id.btnTest);

        btnSignOut = (Button) view.findViewById(R.id.email_sign_out_button);





        mAuth = FirebaseAuth.getInstance();

        btnTEST.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //Intent listMealsIntent = new Intent(getContext(), ActivityTwo.class);
                //getActivity().startActivity(listMealsIntent);
                // Code here executes on main thread after user presses button
                Toast.makeText(getActivity(),"TESTING BUTTON CLICK 1 ", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getContext(),ShopListActivity.class);
                startActivity(intent);
            }
        });

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAuth.signOut();
                toastMessage("Signing Out...");

                goToLogin();
            }
        });



        return view;
    }

    private void goToLogin() {
        Intent intent = new Intent(this.getActivity(),LoginActivity.class);
        startActivity(intent);
    }

    private void toastMessage(String message) {
        Toast.makeText(this.getActivity(),message,Toast.LENGTH_SHORT).show();
    }



}
