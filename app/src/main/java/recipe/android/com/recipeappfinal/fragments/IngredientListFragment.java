package recipe.android.com.recipeappfinal.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.adapter.GridAdapterIngredients;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.CategoryB;
import recipe.android.com.recipeappfinal.model.Ingredient;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 05/08/2017.
 */

public class IngredientListFragment extends Fragment implements TabByIngredientsFragment.DataFromFragmentToFragment {

    private static final String TAG = "IngredientListFragment";

    private List<String> ingredientNamesList = new ArrayList<String>();

    private List<Integer> ingredientIcon = new ArrayList<Integer>();

    private List<CategoryB> lisOfAllCategoriesB;

    List<Recipe> lisOfAlltRecipes;

    GridView gridView;

    // hashMap with multiple values with default size and load factor
    HashMap<String, List<Ingredient>> categoryIngredientsDict;

    private ArrayList<Integer> ingredientsChoosenListId;
    private String currentCategory;
    private List<Recipe> listRecipesResult;


    private TextView badge_notifi_text;
    private ArrayList<Ingredient> ingredientsChoosenList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ingredient_list, container, false);

        badge_notifi_text = (TextView) view.findViewById(R.id.badge_notification_text);

        currentCategory = "";

        ingredientsChoosenList = new ArrayList<Ingredient>();
        ingredientsChoosenListId = new ArrayList<Integer>();

        categoryIngredientsDict = new HashMap<String, List<Ingredient>>();

        // TODO test only insert hardcode in ingredient name and icon
        ingredientNamesList.add("Carne");
        ingredientNamesList.add("Peixe");
        ingredientNamesList.add("Ovo");

        ingredientIcon.add(R.drawable.ic_photo_size_select);
        ingredientIcon.add(R.drawable.ic_photo_size_select);
        ingredientIcon.add(R.drawable.ic_photo_size_select);


        gridView = (GridView) view.findViewById(R.id.gridViewIngredients);

        updateGridView();


        return view;


    }


    private void toastMessage(String message) {

        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void sendData(String data) {

        currentCategory = data;
        updateGridViewByCategory(currentCategory);

    }


    //RECEIVE DATA FROM FRAGMENT TabByIngredientsFragment
    @Override
    public void sendDataList(List<CategoryB> data) {

        List<Ingredient> listIngredients = new ArrayList<Ingredient>();

        if (data != null && !data.isEmpty()) {
            lisOfAllCategoriesB = data;

            for (int i = 0; i < lisOfAllCategoriesB.size(); i++) {
                CategoryB categoryB = lisOfAllCategoriesB.get(i);
                listIngredients = categoryB.getIngredients();
                categoryIngredientsDict.put(categoryB.getNameCategoryIngredientB(), listIngredients);

            }

            ingredientNamesList = new ArrayList<>();
            // TODO test - > text for image and icon  for ingredient
            for (Ingredient ingredient : listIngredients) {
                ingredientNamesList.add(ingredient.getIngredientName());
                ingredientIcon.add(R.drawable.ic_photo_size_select);
            }

            updateGridView();
        }
    }

    private void updateGridViewByCategory(String category) {

        if (category != null && !category.isEmpty() && !category.equalsIgnoreCase("NO DATA")) {

            List<Ingredient> dictValue = categoryIngredientsDict.get(category);
            ingredientNamesList.clear();
            ingredientIcon.clear();
            // TODO test - > text for image and icon  for ingredient
            for (Ingredient ingredient : dictValue) {
                ingredientNamesList.add(ingredient.getIngredientName());
                ingredientIcon.add(R.drawable.ic_photo_size_select);
            }

            updateGridView();

        }
    }


    private void updateGridView() {

        GridAdapterIngredients gridAdapterIngredients = new GridAdapterIngredients(getContext(), ingredientIcon, ingredientNamesList);

        gridView.setAdapter(gridAdapterIngredients);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                toastMessage("Clicked Ingredient");

                addIngredienteToGenerate(position);
                Toast.makeText(getContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addIngredienteToGenerate(int position) {



        //check list of ingredients for spe4cific category
        List<Ingredient> listIngredient = categoryIngredientsDict.get(currentCategory);

        String nameIngredient = ingredientNamesList.get(position);

        if (listIngredient == null) {
            return;
        }

        //check if ingredient selected is in ingredient list and get its ID
        for (int i = 0; i < listIngredient.size(); i++) {
            if (listIngredient.get(i).getIngredientName().equalsIgnoreCase(nameIngredient)) {
                Integer idIingredient = listIngredient.get(i).getIdIngredient();
                //se o id nao estiver na lista addiciona
                if (!ingredientsChoosenListId.contains(idIingredient)) {

                    ingredientsChoosenListId.add(idIingredient); // adds id of ingredients to be generated
                    ingredientsChoosenList.add(listIngredient.get(i)); // adds to list of ingredients to be generated
                }

            }
        }

        TabByIngredientsFragment myParentFragment = (TabByIngredientsFragment) getParentFragment();
        myParentFragment.setBadgeValue(ingredientsChoosenList);


        Log.d(TAG, "onResponse ingredientsChoosenListId : " + ingredientsChoosenListId);
        //getRecipesByIngredients(ingredientsChoosenListId);
    }





}
