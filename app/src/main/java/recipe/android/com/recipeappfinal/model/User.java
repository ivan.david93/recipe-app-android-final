package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedro on 03/06/2017.
 */

public class User  implements Serializable {

    @SerializedName("idUser")
    @Expose
    private Integer idUser;

    @SerializedName("userCategoryList")
    @Expose
    public List<UserCategory> userCategoryList = new ArrayList<UserCategory>();

    public User() {
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public List<UserCategory> getUserCategoryList() {
        return userCategoryList;
    }

    public void setUserCategoryList(List<UserCategory> userCategoryList) {
        this.userCategoryList = userCategoryList;
    }
}
