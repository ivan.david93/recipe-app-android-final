package recipe.android.com.recipeappfinal.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.model.UserDataFirebase;

/**
 * Created by IVAN on 17/08/2017.
 */

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "RegisterActivity";


    private FirebaseAuth firebaseAuth;


    private FirebaseAuth.AuthStateListener mAuthListener;

    private EditText mEmail, mPassword;
    private TextView textSignIn;
    private EditText mPasswordConfirm;
    private EditText userName;

    private Button btnRegister;

    private ProgressDialog progressDialog;


    private DatabaseReference databaseReferenceUsers;

    private RadioGroup radioGroup;
    private String radioGenderChoose;
    private FirebaseUser user;
    private String userID;
    private String UUID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        progressDialog = new ProgressDialog(this);

        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference("users");

        userName = (EditText) findViewById(R.id.username);
        mEmail = (EditText) findViewById(R.id.emailRegister);
        mPassword = (EditText) findViewById(R.id.passwordRegister);
        mPasswordConfirm = (EditText) findViewById(R.id.confirm_passwordRegister);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        textSignIn = (TextView) findViewById(R.id.textAlreadyRegistered);

        radioGenderChoose = "male";


        radioGroup = (RadioGroup) findViewById(R.id.radio_group);

        firebaseAuth = FirebaseAuth.getInstance();

        checkIfLoggedIn(firebaseAuth);


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    //toastMessage("Successfully signed in with: " + user.getEmail());

                    goToMain();
                    //goToLogin();


                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    //toastMessage("Successfully signed out");
                }
                // ...
            }
        };


        btnRegister.setOnClickListener(this);

        textSignIn.setOnClickListener(this);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {



                switch (checkedId) {
                    case R.id.radioMale:
                        // do operations specific to this selection
                        radioGenderChoose = "male";
                        break;
                    case R.id.radioFemale:
                        // do operations specific to this selection
                        radioGenderChoose = "female";
                        break;
                    case R.id.radioNon:
                        // do operations specific to this selection
                        radioGenderChoose = "non-binary";
                        break;
                }

            }
        });


    }


    @Override
    public void onClick(View view) {

        if (view == textSignIn) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        if (view == btnRegister) {
            registerUser();
        }
    }

    private void registerUser() {

        String email = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        String passwordConfirm = mPasswordConfirm.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            //email empty
            toastMessage("Please enter email");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            //password empty
            toastMessage("Please enter password");
            return;
        }

        if (TextUtils.isEmpty(passwordConfirm)) {
            //password empty
            toastMessage("Please enter password confirmation");
            return;
        }

        if (!password.contentEquals(passwordConfirm)) {
            toastMessage("Password confirmation does not match!");
            return;
        }


        //if validations are ok

        progressDialog.setMessage("Registering User...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    //user is successfuly registered and logged in start thge profile activity here
                    toastMessage("Registered Successfully");
                    Log.d(TAG, "onResponse user  : " + user);
                    progressDialog.dismiss();

                }
                if (!task.isSuccessful()) {
                    progressDialog.dismiss();
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e) {
                        mPassword.setError(getString(R.string.error_weak_password));
                        mPassword.requestFocus();
                        Toast.makeText(RegisterActivity.this, "Authentication failed:" + getString(R.string.error_weak_password),
                                Toast.LENGTH_SHORT).show();

                    } catch (FirebaseAuthInvalidCredentialsException e) {
                        mEmail.setError(getString(R.string.error_invalid_email));
                        mEmail.requestFocus();
                        Toast.makeText(RegisterActivity.this, "Authentication failed:" + getString(R.string.error_invalid_email),
                                Toast.LENGTH_SHORT).show();

                    } catch (FirebaseAuthUserCollisionException e) {
                        mEmail.setError(getString(R.string.error_user_exists));
                        mEmail.requestFocus();
                        Toast.makeText(RegisterActivity.this, "Authentication failed:" + getString(R.string.error_user_exists),
                                Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }

                }
            }
        });

    }


    private void getUsersTest() {

        // Get a reference to our posts
        //final FirebaseDatabase database = FirebaseDatabase.getInstance();
        //DatabaseReference ref = database.getReference("users");

// Attach a listener to read the data at our posts reference
        UUID = user.getUid();


        databaseReferenceUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot dsChildren : dataSnapshot.getChildren()) {

                    String nomeUser = dsChildren.child(UUID).getValue(UserDataFirebase.class).getUserName();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

    }

    private void showData(DataSnapshot dataSnapshot) {

        /*
        * to fetch data from database firebase
        *
        * */
        userID = user.getUid();

        for (DataSnapshot dsChildren : dataSnapshot.getChildren()) {

            dsChildren.child(userID).getValue(UserDataFirebase.class).getUserName();

        }

    }

    private void checkIfLoggedIn(FirebaseAuth mAuth) {

        FirebaseUser user = this.firebaseAuth.getCurrentUser();
        if (user != null) {
            this.firebaseAuth.signOut();
        }

    }

    private void goToMain() {


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());

        UserDataFirebase userDataFirebase = new UserDataFirebase();

        String name = userName.getText().toString();
        String email = mEmail.getText().toString();

        //Evaluiate recipe  (recipeId ,rate)
        HashMap<String, String> recipesEval = new HashMap<String, String>();
//        recipesEval.put("1", "4");
//        recipesEval.put("2", "3");
//        recipesEval.put("3", "2");
        //user categories
        List<String> categories = new ArrayList<String>();
//        categories.add("Dummy Category 1");
//        categories.add("Dummy Category 2");
        //set data to pojo

        if (name != null && radioGenderChoose != null && email != null) {


            userDataFirebase.setUserName(name);
            userDataFirebase.setGender(radioGenderChoose);
            userDataFirebase.setAddress("Random Address");
            userDataFirebase.setEmail(email);
            userDataFirebase.setBirthDate(formattedDate);
            //userDataFirebase.setUserId(Integer.toString(idUser));
//        userDataFirebase.setUserCategories(categories);
//        userDataFirebase.setRecipesEvalId(recipesEval);

            databaseReferenceUsers.child(user.getUid()).setValue(userDataFirebase);

            toastMessage("Information Saved. to firebase..");

            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("userDataFirebase", (Serializable) userDataFirebase);
            startActivity(intent);
            finish();
        }
    }

    private void goToLogin() {

        Intent intent = new Intent(this, LoginActivity.class);

        startActivity(intent);
        finish();

    }


    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            firebaseAuth.removeAuthStateListener(mAuthListener);
        }
    }


}

