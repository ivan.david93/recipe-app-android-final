package recipe.android.com.recipeappfinal.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.model.UserDataFirebase;
import recipe.android.com.recipeappfinal.utils.HelperUtils;

/**
 * Created by pedro on 17/08/2017.
 */

public class ActivityListStoragedRecipes extends BaseActivity {

    private static final String TAG = "List Storaged Recipes";

    private static final String RECIPES_CACHE_FOLDER = "recipesFolder";

    List<String> filesNames = null;
    SwipeMenuListView listView;
    File[] files;
    List<File> listOfFiles;

    File currentRecipesDirFolder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_two, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        //TODO DISABLE SHIFT MODE ON BOTTOM IF WANTED HERE
        // BottomNavViewHelper.disableShiftMode(bottomNavigationView);

        // declarare items menu to highlighht each button
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_home_black:
                        Intent intent1 = new Intent(ActivityListStoragedRecipes.this, MainActivity.class);
                        startActivity(intent1);
                        break;
                    case R.id.ic_view_list:
                        Intent intent2 = new Intent(ActivityListStoragedRecipes.this, ActivityTwo.class);
                        startActivity(intent2);
                        break;
                    case R.id.ic_shuffle_black:

                        //dont go to activity 3 generate if server not alive
                        if (HelperUtils.isAlive()) {
                            Intent intent3 = new Intent(ActivityListStoragedRecipes.this, ActivityThree.class);
                            startActivity(intent3);
                            finish();
                        }
                        else{
                            HelperUtils.serverDownMessage(ActivityListStoragedRecipes.this, "Can´t navigate to Generate Recipe");
                        }

                        break;

                }

                return false;

            }
        });

        listView = (SwipeMenuListView) findViewById(R.id.listView);

        File cacheDir = getBaseContext().getCacheDir();
        //Create a File for your desired directory
        //obtem o id do user actualmente autenticado   e.g : "root@gmail.com - > 36 "
        currentRecipesDirFolder = new File(cacheDir, RECIPES_CACHE_FOLDER + "_" + HelperUtils.getUserDataFirebase().getUserId());


        //Call mkdirs() on that File to create the directory if it does not exist
        if (!currentRecipesDirFolder.exists()) {
            //directory is created;
            currentRecipesDirFolder.mkdir();
        }

        if (currentRecipesDirFolder.listFiles().length == 0) {
            TextView emptyList = (TextView) findViewById(R.id.activityTitle1);
            emptyList.setText("Não existem listas geradas");

        } else {

            files = currentRecipesDirFolder.listFiles();

            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });

   /*         try {
                recipeList = readRecipesFromInternalMemory(files[files.length - 1].getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }*/

            listOfFiles = new ArrayList<File>(Arrays.asList(files));

            filesNames = new ArrayList<String>();
            for (File fileStorage : listOfFiles) {
                //String fileName = convertFileNameToStringApp(fileStorage.getName());
                filesNames.add(fileStorage.getName());
            }


            ArrayAdapter adapter = new ArrayAdapter(ActivityListStoragedRecipes.this, android.R.layout.simple_list_item_1, filesNames);
            listView.setAdapter(adapter);


            SwipeMenuCreator creator = new SwipeMenuCreator() {

                @Override
                public void create(SwipeMenu menu) {
                    // create "open" item
                    SwipeMenuItem openItem = new SwipeMenuItem(
                            getApplicationContext());
                    // set item background
                    openItem.setBackground(new ColorDrawable(Color.rgb(0x00, 0x66,
                            0xff)));
                    // set item width
                    openItem.setWidth(170);
                    // set item title
                    openItem.setTitle("Open");
                    // set item title fontsize
                    openItem.setTitleSize(18);
                    // set item title font color
                    openItem.setTitleColor(Color.WHITE);
                    // add to menu
                    menu.addMenuItem(openItem);

                    // create "delete" item
                    SwipeMenuItem deleteItem = new SwipeMenuItem(
                            getApplicationContext());
                    // set item background
                    deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                            0x3F, 0x25)));
                    // set item width
                    deleteItem.setWidth(170);
                    // set a icon
                    deleteItem.setIcon(R.drawable.ic_delete);
                    // add to menu
                    menu.addMenuItem(deleteItem);
                }
            };

            listView.setMenuCreator(creator);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent intentShowFileSelected = new Intent(ActivityListStoragedRecipes.this, ActivityTwo.class);
                    intentShowFileSelected.putExtra("storagedFileSelected", listOfFiles.get(position));
                    startActivity(intentShowFileSelected);
                }
            });


            listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            Log.d(TAG, "clicked item " + index);
                            //File currentDB = new File("/data/data/" + getApplicationContext().getPackageName() + "/");

                            //Log.d(TAG, "Open----------------> ssdsdaasdasas " );//+ currentDB.listFiles()[5].delete());

                            //boolean success = (new File(currentDB.listFiles()[5].getAbsolutePath())).delete();

                            //Recipe recipeSelected = recipeList.get(position);
                            //startActivtyRecipeDetail(recipeSelected);
                            break;
                        case 1:
                            final AlertDialog alert = new AlertDialog.Builder(ActivityListStoragedRecipes.this).create();
                            alert.setTitle("Warning");
                            alert.setMessage("Do you want to delete this recipe list storaged ?");
                            alert.setButton(Dialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Log.d(TAG, "onMenuItemClick: clicked item " + position);
                                    listOfFiles.get(position).delete();
                                    listOfFiles.remove(position);
                                    filesNames.remove(position);

                                    ArrayAdapter adapter = new ArrayAdapter(ActivityListStoragedRecipes.this, android.R.layout.simple_list_item_1, filesNames);
                                    listView.setAdapter(adapter);

                                }
                            });
                            alert.setButton(Dialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //finish();
                                }
                            });
                            alert.show();

                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });


        }


    }



    private void writeRecipesFromInternalMemory(String params, List<Recipe> recipes) throws IOException {

        FileOutputStream outStream = new FileOutputStream("/data/data/" + getApplicationContext().getPackageName() + "/files/" + params);
        ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
        objectOutStream.writeInt(recipes.size()); // Save size first
        for (Recipe recipe : recipes)
            objectOutStream.writeObject(recipe);
        objectOutStream.close();
    }

    private List<Recipe> readRecipesFromInternalMemory(String absolutePath) throws IOException, ClassNotFoundException {


        //FileInputStream inStream = new FileInputStream("/data/data/" + getApplicationContext().getPackageName() + "/files/" + params);
        FileInputStream inStream = new FileInputStream(absolutePath);


        ObjectInputStream objectInStream = new ObjectInputStream(inStream);
        int count = objectInStream.readInt(); // Get the number of regions
        List<Recipe> listOfStoragedRecipe = new ArrayList<Recipe>();
        for (int c = 0; c < count; c++)
            listOfStoragedRecipe.add((Recipe) objectInStream.readObject());
        objectInStream.close();

        //Log.d(TAG, "=============== LISTA FINALLL ==> " + rl.toString());
        return listOfStoragedRecipe;
    }

    private String convertFileNameToStringApp(String fileName) {
        StringBuilder myFileName = new StringBuilder(fileName);
        for (int i = 0; i < fileName.length(); i++) {
            if (i == 7 || i == 12 || i == 23) {
                myFileName.setCharAt(i, ' ');
            }
            if (i == 15 || i == 18) {
                myFileName.setCharAt(i, '/');
            }
            if (i == 26 || i == 29) {
                myFileName.setCharAt(i, ':');
            }
        }
        return myFileName.toString();
    }


    /* (non-Javadoc)
  * @see android.app.Activity#onDestroy()
  */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "On Destroy .....");
    }
    /* (non-Javadoc)
    * @see android.app.Activity#onPause()
    */
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "On Pause .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onRestart()
    */
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "On Restart .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onResume()
    */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "On Resume .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onStart()
    */
    @Override
    protected void onStart() {
        super.onStart();

        Log.i(TAG, "On Start .....");
        HelperUtils.checkIsServerAlive();
    }
    /* (non-Javadoc)
    * @see android.app.Activity#onStop()
    */
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "On Stop .....");
    }
}

