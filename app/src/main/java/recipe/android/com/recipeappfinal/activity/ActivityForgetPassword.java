package recipe.android.com.recipeappfinal.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import recipe.android.com.recipeappfinal.R;

/**
 * Created by IVAN on 23/08/2017.
 */

public class ActivityForgetPassword extends BaseActivity implements View.OnClickListener {

    private EditText input_email;
    private Button btnResetPass;

    private FirebaseAuth auth;
    private ConstraintLayout activity_forgot;
    private ImageView backBtn;

    private ProgressDialog progressDialog;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        input_email = (EditText) findViewById(R.id.forgot_email);
        btnResetPass = (Button) findViewById(R.id.forgot_btn_reset);
        activity_forgot = (ConstraintLayout) findViewById(R.id.activity_forget_password);

        backBtn = (ImageView) findViewById(R.id.backBtn);

        btnResetPass.setOnClickListener(this);
        backBtn.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);

        //init firebase
        auth = FirebaseAuth.getInstance();


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.forgot_btn_reset:
                resetPassword(input_email.getText().toString());
                break;

            case R.id.backBtn:
                goBack();
                break;

        }

    }

    private void resetPassword(final String email) {

        progressDialog.setMessage("Processing...");
        progressDialog.show();

        auth.sendPasswordResetEmail(email).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    progressDialog.dismiss();
                    btnResetPass.setEnabled(false);

                    snackbar = Snackbar.make(activity_forgot, "We have sent password to email: " + email, Snackbar.LENGTH_SHORT);

                    show();
                    //goBack();
                } else {
                    progressDialog.dismiss();
                    snackbar = Snackbar.make(activity_forgot, "Failed to sent password" + email, Snackbar.LENGTH_SHORT);
                    show();
                    //goBack();
                }
                //progressDialog.dismiss();
            }
        });

    }

    private void show() {

        snackbar.show();
    }

    private void goBack() {
        finish();
    }
}
