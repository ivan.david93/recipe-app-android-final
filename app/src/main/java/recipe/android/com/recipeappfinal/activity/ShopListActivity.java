package recipe.android.com.recipeappfinal.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.adapter.ShopListAdapter;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.IngredientHelper;
import recipe.android.com.recipeappfinal.model.Measure;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by IVAN on 18/08/2017.
 */

public class ShopListActivity extends AppCompatActivity implements View.OnClickListener {

    //  shortcut   --- logt
    private static final String TAG = "ShopListActivity";
    private List<IngredientHelper> listRecipesIngredients;
    private ListView mListView;
    private ImageView backBtn;
    private File currentRecipesDirFolder;

    private static final String RECIPES_CACHE_FOLDER = "recipesFolder";
    private File fileSelected;
    private File[] files;
    private List<Recipe> recipeList;
    List<Integer> idRecipes;
    private TextView emptyListText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list);

        mListView = (ListView) findViewById(R.id.ingredientShopListView);

        backBtn = (ImageView) findViewById(R.id.backBtn);

        idRecipes = new ArrayList<>();

        emptyListText = (TextView) findViewById(R.id.text_no_ingredients);

        // ONLY FOR TESTING
        IngredientHelper ingredient_0 = new IngredientHelper(1, "Cebola", new Measure(1, "Kilogram", "kg"), 20);
        IngredientHelper ingredient_1 = new IngredientHelper(1, "Cebola", new Measure(2, "Kilogram", "kg"), 10);
        IngredientHelper ingredient_2 = new IngredientHelper(1, "Cebola", new Measure(3, "Kilogram", "kg"), 22);
        IngredientHelper ingredient_3 = new IngredientHelper(1, "Cebola", new Measure(4, "Kilogram", "kg"), 4);
        IngredientHelper ingredient_4 = new IngredientHelper(1, "Cebola", new Measure(1, "Kilogram", "kg"), 2);
        IngredientHelper ingredient_5 = new IngredientHelper(1, "Cebola", new Measure(1, "Kilogram", "kg"), 1);
        IngredientHelper ingredient_6 = new IngredientHelper(1, "Cebola", new Measure(1, "Kilogram", "kg"), 200);

        // ADD THE INGREDIENT TO ARRAYLIST
        List<IngredientHelper> ingredientHelperList = new ArrayList<>();
        ingredientHelperList.add(ingredient_0);
        ingredientHelperList.add(ingredient_1);
        ingredientHelperList.add(ingredient_2);
        ingredientHelperList.add(ingredient_3);
        ingredientHelperList.add(ingredient_4);
        ingredientHelperList.add(ingredient_5);
        ingredientHelperList.add(ingredient_6);

        //TODO
        //CHECK/GET LISTED RECIPES BY THE USER
        // SERVER LOGIC --- GET ALL INGREDIENTS FOR EACH RECIPE
        //SERVER LOGIC --- IF INGREDIENTS EQUAL SUM INGREDIENT QUANTITY
        // RETRIEVE ArrayList<IngredientHelper> listRecipesIngredients

        //ShopListAdapter adapter =  new ShopListAdapter(this,R.layout.adapter_view_shop_layout,ingredientHelperList);
        //TODO list is hardcoded need to pass the recipes that are currentlyu active for user
        //TEST
//        idRecipes.add(1);
//        idRecipes.add(2);
//        idRecipes.add(3);

        backBtn.setOnClickListener(this);

        showListRecipes();
    }

    /*
    *
    * TO GET ALL THHE INGREDIENTS MERGED FROM THE GENERATEED RECIPES
    *
    * */
    private void getIngredientsFromRecipes(List<Integer> listIdRecipes) {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<IngredientHelper>> call = taskService.getIngredientsFromRecipes(listIdRecipes);
        call.enqueue(new Callback<List<IngredientHelper>>() {

            @Override
            public void onResponse(Call<List<IngredientHelper>> call, Response<List<IngredientHelper>> response) {

                listRecipesIngredients = response.body();

                if (listRecipesIngredients != null && !listRecipesIngredients.isEmpty()) {
                    Log.d(TAG, "onResponse ingredientList merged ingredients ----------> " + listRecipesIngredients);

                    //TODO LIST TO BE USED AT THE ADAPTER
                    buildAdapterInfo();

                }
            }

            @Override
            public void onFailure(Call<List<IngredientHelper>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(TAG, "Error getIngredientsFromRecipes");
            }
        });
    }

    private void buildAdapterInfo() {

        //TODO to be used this list
        ShopListAdapter adapter = new ShopListAdapter(this, R.layout.adapter_view_shop_layout, listRecipesIngredients);

        mListView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.backBtn:
                Intent intent1 = new Intent(ShopListActivity.this, MainActivity.class);
                startActivity(intent1);
                finish();

                break;

        }

    }


    private void showListRecipes() {

        File cacheDir = getBaseContext().getCacheDir();
        //Create a File for your desired directory
        currentRecipesDirFolder = new File(cacheDir, RECIPES_CACHE_FOLDER + "_" + HelperUtils.getUserDataFirebase().getUserId());

        //Call mkdirs() on that File to create the directory if it does not exist
        //Call mkdirs() on that File to create the directory if it does not exist
        if (!currentRecipesDirFolder.exists()) {
            //directory is created;
            currentRecipesDirFolder.mkdir();
        }

        //currentRecipesDirFolder = new File("/data/data/" + getContext().getPackageName() + "/files");
        int numOfStorageRecipes = currentRecipesDirFolder.listFiles().length;

        if (numOfStorageRecipes == 0) {
//            TextView emptyList = (TextView) findViewById(R.id.text_no_ingredients);
            emptyListText.setText("Não existem listas geradas");
        } else {

            files = currentRecipesDirFolder.listFiles();

            Arrays.sort(files, new Comparator<File>() {
                public int compare(File f1, File f2) {
                    return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                }
            });
            try {
                fileSelected = files[files.length - 1];
                recipeList = readRecipesFromInternalMemory(fileSelected.getAbsolutePath());


            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }

        if (recipeList != null && !recipeList.isEmpty()) {
            for (Recipe recipe : recipeList) {
                idRecipes.add(recipe.getIdRecipe());
            }
            //T
            getIngredientsFromRecipes(idRecipes);
        }

    }

    private List<Recipe> readRecipesFromInternalMemory(String absolutePath) throws IOException, ClassNotFoundException {


        //FileInputStream inStream = new FileInputStream("/data/data/" + getApplicationContext().getPackageName() + "/files/" + params);
        FileInputStream inStream = new FileInputStream(absolutePath);


        ObjectInputStream objectInStream = new ObjectInputStream(inStream);
        int count = objectInStream.readInt(); // Get the number of regions
        List<Recipe> listOfStoragedRecipe = new ArrayList<Recipe>();
        for (int c = 0; c < count; c++)
            listOfStoragedRecipe.add((Recipe) objectInStream.readObject());
        objectInStream.close();

        //Log.d(TAG, "=============== LISTA FINALLL ==> " + rl.toString());
        return listOfStoragedRecipe;
    }
}
