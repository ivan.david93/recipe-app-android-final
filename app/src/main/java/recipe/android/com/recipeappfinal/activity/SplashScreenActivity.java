package recipe.android.com.recipeappfinal.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.utils.HelperUtils;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView textView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        textView = (TextView) findViewById(R.id.textViewSplash);
        imageView = (ImageView) findViewById(R.id.imageViewSplash);

        Animation myAnimation = AnimationUtils.loadAnimation(this, R.anim.mytransition);
        textView.startAnimation(myAnimation);
        imageView.startAnimation(myAnimation);
        final Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(intent);
                    finish();
                }
            }
        };
        timer.start();

        //CHECK IF NETWORK AVAILABLE
      /*  if (!isNetworkAvailable()) {
            //create an alert dialog for
            AlertDialog.Builder CheckBuilder = new AlertDialog.Builder(SplashScreenActivity.this);
            CheckBuilder.setTitle("Error!");
            CheckBuilder.setMessage("Please check your internet connection.");
            //Buil Retry Button
            CheckBuilder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //restart activity
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });
            CheckBuilder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //finish activiyt
                    finish();
                }
            });
            CheckBuilder.setCancelable(false);
            AlertDialog alert = CheckBuilder.create();
            alert.show();

        } else {
            //CHECK IF CONNECTION TO SERVER
            //if not do this
            if (!HelperUtils.isServerAlive()) {
                //create an alert dialog for
                AlertDialog.Builder CheckBuilder = new AlertDialog.Builder(SplashScreenActivity.this);
                CheckBuilder.setTitle("Error!");
                CheckBuilder.setMessage("Server is down. Please retry later");
                //Buil Retry Button
                CheckBuilder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish activity
                        finish();
                    }
                });
                CheckBuilder.setCancelable(false);
                AlertDialog alert = CheckBuilder.create();
                alert.show();
            }
        }*/
    }


    private boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null;
    }


}
