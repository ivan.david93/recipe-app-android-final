package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by IVAN on 15/08/2017.
 */

public class IngredientHelper  implements Serializable {

    @SerializedName("idIngredient")
    @Expose
    private Integer idIngredient;

    @SerializedName("ingredientName")
    @Expose
    private String ingredientName;

    @SerializedName("measure")
    @Expose
    private Measure measure;

    @SerializedName("quantity")
    @Expose
    private int quantity;


    public IngredientHelper(Integer idIngredient, String ingredientName, Measure measure, int quantity) {
        this.idIngredient = idIngredient;
        this.ingredientName = ingredientName;
        this.measure = measure;
        this.quantity = quantity;
    }

    public IngredientHelper(String ingrName, Measure ingrMeasure, String ingrQuant) {
    }

    public Integer getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(Integer idIngredient) {
        this.idIngredient = idIngredient;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public Measure getMeasure() {
        return measure;
    }

    public void setMeasure(Measure measure) {
        this.measure = measure;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
