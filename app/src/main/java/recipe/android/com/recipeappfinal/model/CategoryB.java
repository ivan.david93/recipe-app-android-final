package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pedro on 03/06/2017.
 */

 public class CategoryB  implements Serializable {

    @SerializedName("idCategoryIngredientB")
    @Expose
    private Integer idCategoryIngredientB;

    @SerializedName("nameCategoryIngredientB")
    @Expose
    private String nameCategoryIngredientB;

    @SerializedName("categoryIngredientA")
    @Expose
   private CategoryA categoryIngredientA;

    @SerializedName("ingredients")
    @Expose
   private List<Ingredient> ingredients = new ArrayList<Ingredient>();

    public CategoryB() {
    }

    public Integer getIdCategoryIngredientB() {
        return idCategoryIngredientB;
    }

    public void setIdCategoryIngredientB(Integer idCategoryIngredientB) {
        this.idCategoryIngredientB = idCategoryIngredientB;
    }

    public String getNameCategoryIngredientB() {
        return nameCategoryIngredientB;
    }

    public void setNameCategoryIngredientB(String nameCategoryIngredientB) {
        this.nameCategoryIngredientB = nameCategoryIngredientB;
    }

    public CategoryA getCategoryIngredientA() {
        return categoryIngredientA;
    }

    public void setCategoryIngredientA(CategoryA categoryIngredientA) {
        this.categoryIngredientA = categoryIngredientA;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }


    @Override
    public String toString() {
        return "CategoryB{" +
                "idCategoryIngredientB=" + idCategoryIngredientB +
                ", nameCategoryIngredientB='" + nameCategoryIngredientB + '\'' +
                ", categoryIngredientA=" + categoryIngredientA +
                ", ingredients=" + ingredients +
                '}';
    }
}
