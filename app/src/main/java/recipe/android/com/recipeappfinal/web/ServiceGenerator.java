package recipe.android.com.recipeappfinal.web;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonParseException;

import java.text.DateFormat;
import java.util.Date;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by pedro on 03/06/2017.
 */

public class ServiceGenerator {

    private static final String BASE_URL = "http://194.210.196.83:8080/"; // TODO MY IP
    //private static final String BASE_URL = "http://recipesapp.com:8080/"; // TODO MY IP
    //private static final String BASE_URL = "http://192.168.1.79:8080/"; // TODO Quebrada IP
    //private static final String BASE_URL = "http://192.168.1.85:8080/"; // TODO BENTO IP

    private static Gson gson = new GsonBuilder()
            .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
            .create();


    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    private static Retrofit retrofit = builder.build();

    public static <S> S createService(
            Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

}
