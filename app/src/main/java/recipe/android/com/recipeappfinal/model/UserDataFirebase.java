package recipe.android.com.recipeappfinal.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by IVAN on 17/08/2017.
 */

public class UserDataFirebase implements Serializable {

    private String userName;
    private String email;
    private String gender;
    private String userId;
    private String address;
    private String birthDate;


    private List<String> userCategories;
    //Evaluiate recipe  (recipeId ,rate)
    private HashMap<String,String> recipesEvalId;

    private  String updateDate;
    private List<String> favouritRecipes = new ArrayList<>();

    public UserDataFirebase() {
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public HashMap<String, String> getRecipesEvalId() {
        return recipesEvalId;
    }

    public void setRecipesEvalId(HashMap<String, String> recipesEvalId) {
        this.recipesEvalId = recipesEvalId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public List<String> getUserCategories() {
        return userCategories;
    }

    public void setUserCategories(List<String> userCategories) {
        this.userCategories = userCategories;
    }

    public List<String> getFavouritRecipes() {
        return favouritRecipes;
    }

    public void setFavouritRecipes(List<String> favouritRecipes) {
        this.favouritRecipes = favouritRecipes;
    }
}
