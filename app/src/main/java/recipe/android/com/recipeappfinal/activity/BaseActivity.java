package recipe.android.com.recipeappfinal.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.utils.HelperUtils;

/**
 * Created by pedro on 17/08/2017.
 */

public class BaseActivity extends AppCompatActivity {

    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        mAuth = FirebaseAuth.getInstance();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                final String appPackageName = getPackageName();

                switch (item.getItemId()) {

                    case R.id.nav_account_settings:

                        if (HelperUtils.isAlive()) {
                            Intent storageAccSettingsIntent = new Intent(getApplicationContext(), ProfileScreenXMLUIDesign.class);
                            startActivity(storageAccSettingsIntent);
                            //toastMessage("nav_account_settings...");

                        } else {
                            HelperUtils.serverDownMessage(BaseActivity.this, "Can´t navigate to account settings");
                        }
                        drawerLayout.closeDrawers();
                        break;


                    case R.id.nav_stored_receipts:
                        Intent storageRecipesIntent = new Intent(getApplicationContext(), ActivityListStoragedRecipes.class);
                        startActivity(storageRecipesIntent);
//                        finish();
                        toastMessage("nav_stored_receipts...");
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.nav_shop_list:
                        if (HelperUtils.isAlive()) {
                            Intent shopList = new Intent(getApplicationContext(), ShopListActivity.class);
                            startActivity(shopList);
                        } else {
                            HelperUtils.serverDownMessage(BaseActivity.this, "Can´t navigate to sjop list");
                        }
//                        finish();
                        //toastMessage("nav_stored_receipts...");
                        drawerLayout.closeDrawers();
                        break;

                    case R.id.nav_logout:

                        mAuth.signOut();
                        toastMessage("Signing Out...");

                        goToLogin();
                        //Intent anIntent = new Intent(getApplicationContext(), ActivityTwo.class);
                        //startActivity(anIntent);
//                        finish();
                        drawerLayout.closeDrawers();
                        break;

                }
                return false;
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        actionBarDrawerToggle.syncState();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    private void goToLogin() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void toastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }
}