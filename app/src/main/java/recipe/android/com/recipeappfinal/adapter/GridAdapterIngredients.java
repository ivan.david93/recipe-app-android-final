package recipe.android.com.recipeappfinal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import recipe.android.com.recipeappfinal.R;

/**
 * Created by IVAN on 10/08/2017.
 */

public class GridAdapterIngredients extends BaseAdapter {

    private List<Integer> icons;

    private List<String> ingredientNames;

    private Context context;

    private LayoutInflater inflater;

    public GridAdapterIngredients(Context context,  List<Integer> icons, List<String> ingredientNames) {
        this.context = context;
        this.icons = icons;
        this.ingredientNames = ingredientNames;

    }


    @Override
    public int getCount() {
        return ingredientNames.size();
    }

    @Override
    public Object getItem(int position) {
        return ingredientNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        View gridView = convertView;

        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            gridView = inflater.inflate(R.layout.custom_ingredient_layout, null);
        }

        ImageView icon = (ImageView) gridView.findViewById(R.id.imageButtonIngredient);
        TextView ingredientName = (TextView) gridView.findViewById(R.id.textViewIngredientName);

        icon.setImageResource(icons.get(position));
        ingredientName.setText(ingredientNames.get(position));

        return gridView;
    }
}
