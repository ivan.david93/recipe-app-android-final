package recipe.android.com.recipeappfinal.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.model.IngredientHelper;
import recipe.android.com.recipeappfinal.model.Measure;

/**
 * Created by IVAN on 18/08/2017.
 */

public class ShopListAdapter extends ArrayAdapter<IngredientHelper> {

    private static final String TAG = "ShopListAdapter";
    private final int mResource;


    private Context mContext;


    public ShopListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<IngredientHelper> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //get INgredient Info
        String ingrName = getItem(position).getIngredientName();
        String ingrQuant = String.valueOf(getItem(position).getQuantity());
        Measure ingrMeasure = getItem(position).getMeasure();


        //create the inggredn obbjecto with info
        IngredientHelper ingredient_0 = new IngredientHelper(ingrName,ingrMeasure, ingrQuant);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);


        TextView tvingrName = (TextView) convertView.findViewById(R.id.nameIngredient);
        TextView tvingrQuant = (TextView) convertView.findViewById(R.id.textView_qnt);
        TextView tvingrMeasure = (TextView) convertView.findViewById(R.id.textView_measure);

        tvingrName.setText(ingrName);
        tvingrQuant.setText(ingrQuant);
        tvingrMeasure.setText(ingrMeasure.getInitialsMeasure());

        return convertView;
    }
}
