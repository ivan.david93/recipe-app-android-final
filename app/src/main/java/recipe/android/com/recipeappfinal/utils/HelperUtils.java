package recipe.android.com.recipeappfinal.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.UserDataFirebase;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 16/08/2017.
 */


public class HelperUtils {

    private static TextView textViewUserNameProfL;

    private static DatabaseReference databaseReferenceUsers;

    private static FirebaseUser firebaseUser;

    private static boolean isAlive = false;

    private static String userUID;

    private static UserDataFirebase userDataFirebase;


    public static FirebaseUser getFirebaseUser() {
        return firebaseUser;
    }

    public static void setFirebaseUser(FirebaseUser firebaseUser) {
        HelperUtils.firebaseUser = firebaseUser;
    }

    public static DatabaseReference getDatabaseReferenceUsers() {
        return databaseReferenceUsers;
    }

    public static void setDatabaseReferenceUsers(DatabaseReference databaseReferenceUsers) {
        HelperUtils.databaseReferenceUsers = databaseReferenceUsers;
    }

    public String getUserUID() {
        return userUID;
    }

    public static void setUserUID(String userID) {
        HelperUtils.userUID = userID;
    }

    public static UserDataFirebase getUserDataFirebase() {
        return userDataFirebase;
    }

    public static void setUserDataFirebase(UserDataFirebase userDataFirebase) {
        HelperUtils.userDataFirebase = userDataFirebase;
    }

    public static boolean isAlive() {

        return isAlive;
    }

    public static void setIsAlive(boolean isAlive) {

        HelperUtils.isAlive = isAlive;
    }


    public static void checkIsServerAlive() {
        setIsAlive(false);

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<Boolean> call = taskService.isServerAlive();
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {

                if (response.isSuccessful()) {
                    // Do awesome stuff
                    //isAlive[0] = true;
                    setIsAlive(true);

                } else if (response.code() == 401) {
                    // Handle unauthorized
                } else {
                    // Handle other responses
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                // something went completely south (like no internet connection)
                setIsAlive(false);
                Log.d("HelperUtils", "Error Server Alive");
            }
        });

    }


    public static void serverDownMessage(Context context, String message) {
        //create an alert dialog for
        AlertDialog.Builder CheckBuilder = new AlertDialog.Builder(context);
        CheckBuilder.setMessage("No connection to server: " + message);
        //Buil Retry Button
        CheckBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //DO NOTHING

            }
        });
        CheckBuilder.setCancelable(false);
        AlertDialog alert = CheckBuilder.create();
        alert.show();
    }

    /*
    * THIS IS A HELPER CLASS THAT GET DATA FROM FIREBASE AND SAVE USER TO BE USED AT EVERY CLASS AND CONTEXT
    * */
    public static UserDataFirebase getCurrentUserDataFromFireBase(final FirebaseUser user, final Activity activity) {

        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference("users");

        databaseReferenceUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                userUID = user.getUid();
                userDataFirebase = dataSnapshot.child(userUID).getValue(UserDataFirebase.class);

                textViewUserNameProfL = (TextView) activity.findViewById(R.id.textViewUserNameProfile);

                if (userDataFirebase != null) {

                    textViewUserNameProfL.setText(userDataFirebase.getUserName());

                    setUserDataFirebase(userDataFirebase);
                }
                setFirebaseUser(user);

                //showData(user, dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return userDataFirebase;
    }

    private static void showData(FirebaseUser user, DataSnapshot dataSnapshot) {
        /*
        * to fetch data from database firebase
        *
        * */
        userUID = user.getUid();
//        for (DataSnapshot dsChildren : dataSnapshot.getChildren()) {
        userDataFirebase = dataSnapshot.child(userUID).getValue(UserDataFirebase.class);
        setUserDataFirebase(userDataFirebase);
//        }
    }


    public static void toastMessage(Context context , String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
