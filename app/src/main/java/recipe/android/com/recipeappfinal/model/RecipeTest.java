package recipe.android.com.recipeappfinal.model;

/**
 * Created by IVAN on 05/08/2017.
 */

public class RecipeTest {

    private String recipeName;
    private String recipeDescr;
    private String recipeTime;

    public RecipeTest(String recipeName, String recipeDescr, String recipeTime) {
        this.recipeName = recipeName;
        this.recipeDescr = recipeDescr;
        this.recipeTime = recipeTime;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipeDescr() {
        return recipeDescr;
    }

    public void setRecipeDescr(String recipeDescr) {
        this.recipeDescr = recipeDescr;
    }

    public String getRecipeTime() {
        return recipeTime;
    }

    public void setRecipeTime(String recipeTime) {
        this.recipeTime = recipeTime;
    }
}
