package recipe.android.com.recipeappfinal.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.model.Recipe;

/**
 * Created by IVAN on 05/08/2017.
 */

public class TabListRecipeFragment extends Fragment {

    private static final String TAG = "Tab3Fragment";

    private Button btnTEST;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment4_layout, container, false);

        SwipeMenuListView listView = (SwipeMenuListView) view.findViewById(R.id.listView);

        Intent reciverRecipeListIntent = getActivity().getIntent();
        ArrayList<Recipe> recipeList = (ArrayList<Recipe>) reciverRecipeListIntent.getSerializableExtra("lisOfAlltRecipes");

        /*ArrayList<String> list = new ArrayList<>();
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");
        list.add("mitch");*/


        ArrayAdapter adapter = new ArrayAdapter(view.getContext(), android.R.layout.simple_list_item_1, recipeList);
        listView.setAdapter(adapter);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(view.getContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0x00, 0x66,
                        0xff)));
                // set item width
                openItem.setWidth(170);
                // set item title
                openItem.setTitle("Open");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        view.getContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(170);
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        listView.setMenuCreator(creator);

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        Log.d(TAG, "onMenuItemClick: clicked item " + index);
                        break;
                    case 1:
                        Log.d(TAG, "onMenuItemClick: clicked item " + index);
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        return view;
    }
}


       /* ListView listView = (ListView) view.findViewById(R.id.list_view_recipes);

        // Create Recipe Objects
        RecipeTest recipeOne = new RecipeTest("Arroz de Pato" , "Isto é Arroz de Pato!" ,"10 min");
        RecipeTest recipeTwo = new RecipeTest("Arroz de Galinha" , "Isto é Arroz de Galinha!" ,"15 min");
        RecipeTest recipeThree = new RecipeTest("Cabidela" , "Isto é Cabidela!" ,"10 min");
        RecipeTest recipeFour = new RecipeTest("Bacalhau a Braz" , "Isto é Bacalhau a Braz!" ,"10 min");
        RecipeTest recipe5 = new RecipeTest("Omelete" , "Isto é Omelete!" ,"10 min");
        RecipeTest recipe6 = new RecipeTest("Alho Frances com Ovo descaido" , "Isto éAlho Frances com Ovo descaido!" ,"10 min");
        RecipeTest recipe7 = new RecipeTest("Francesinha" , "Isto é Francesinha!" ,"8 min");
        RecipeTest recipe8 = new RecipeTest("Atum com arroz" , "Isto éAtum com arroz!" ,"9 min");
        RecipeTest recipe9 = new RecipeTest("Bolonhesa" , "Isto é Bolonhesa!" ,"7 min");
        RecipeTest recipe10 = new RecipeTest("Lasanha" , "Isto é Lasanha!" ,"6 min");
        RecipeTest recipe11 = new RecipeTest("Pizza" , "Isto é Pizza!" ,"5 min");

        //Add recipes to array list
        ArrayList<RecipeTest> recipeList = new ArrayList<>();
        recipeList.add(recipeOne);
        recipeList.add(recipeTwo);
        recipeList.add(recipeThree);
        recipeList.add(recipeFour);
        recipeList.add(recipe5);

        recipeList.add(recipe6);
        recipeList.add(recipe7);
        recipeList.add(recipe8);
        recipeList.add(recipe9);
        recipeList.add(recipe10);

        recipeList.add(recipe11);

        RecipeListAdapter adapter = new RecipeListAdapter(this.getActivity(),R.layout.adapter_view_layout,recipeList);
        listView.setAdapter(adapter);


        return view;

*/

