package recipe.android.com.recipeappfinal.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 05/08/2017.
 */

public class ActivityTwo extends BaseActivity {


    private static final String TAG = "Activity 2";
    List<Recipe> recipeList = null;
    List<String> recipeNames = null;
    SwipeMenuListView listView;
    File currentRecipesDirFolder;
    File[] files;
    File fileSelected;

    private static final String RECIPES_CACHE_FOLDER = "recipesFolder";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_two, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        //TODO DISABLE SHIFT MODE ON BOTTOM IF WANTED HERE
        // BottomNavViewHelper.disableShiftMode(bottomNavigationView);

        // declarare items menu to highlighht each button
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        listView = (SwipeMenuListView) findViewById(R.id.listView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_home_black:
                        Intent intent1 = new Intent(ActivityTwo.this, MainActivity.class);
                        startActivity(intent1);
                        finish();
                        break;
                    case R.id.ic_view_list:
                        Intent intent2 = new Intent(ActivityTwo.this, ActivityTwo.class);
                        startActivity(intent2);
                        finish();
                        break;
                    case R.id.ic_shuffle_black:
                        //dont go to activity 3 generate if server not alive
                        if (HelperUtils.isAlive()) {
                            Intent intent3 = new Intent(ActivityTwo.this, ActivityThree.class);
                            startActivity(intent3);
                            finish();
                        } else {
                            HelperUtils.serverDownMessage(ActivityTwo.this, "Can´t navigate to Generate Recipe");
                        }
                        break;

                }
                return false;

            }
        });

        showListRecipes();

    }


    public void mCreateAndSaveFile(String params, String mJsonResponse) {
        try {
            FileWriter file = new FileWriter("/data/data/" + getApplicationContext().getPackageName() + "/" + params);
            file.write(mJsonResponse);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void startActivtyRecipeDetail(Recipe recipe) {

        Intent intent1 = new Intent(ActivityTwo.this, ActivityRecipeDetail.class);

        intent1.putExtra("recipeObj", recipe);

        startActivity(intent1);
    }

    private void writeRecipesFromInternalMemory(List<Recipe> recipes) throws IOException {


        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        String strDate = sdf.format(cal.getTime());

        /*FileOutputStream outStream = new FileOutputStream("/data/data/" + getApplicationContext().getPackageName() + "/files/Storage_Date_" + strDate);

        ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
        objectOutStream.writeInt(recipes.size()); // Save size first
        for (Recipe recipe : recipes)
            objectOutStream.writeObject(recipe);
        objectOutStream.close();*/

        String FILE_NAME = "recipeList_storage_date_" + strDate;

        File cacheDir = getBaseContext().getCacheDir();
        //Create a File for your desired directory
        File direct = new File(cacheDir, RECIPES_CACHE_FOLDER + "_"+ HelperUtils.getUserDataFirebase().getUserId());
        //Call mkdirs() on that File to create the directory if it does not exist
        if (!direct.exists()) {
            if (direct.mkdir()) ; //directory is created;
        }
        //Create a File for the output file
        File file = new File(direct, FILE_NAME);

        FileOutputStream outStream;

        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            //commented line throws an exception if filename contains a path separator
            //outstream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outStream = new FileOutputStream(file);
//            outstream.write(message.getBytes());
//            outstream.close();
            ObjectOutputStream objectOutStream = new ObjectOutputStream(outStream);
            objectOutStream.writeInt(recipes.size()); // Save size first
            for (Recipe recipe : recipes) {
                objectOutStream.writeObject(recipe);
            }
            objectOutStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Recipe> readRecipesFromInternalMemory(String absolutePath) throws IOException, ClassNotFoundException {


        //FileInputStream inStream = new FileInputStream("/data/data/" + getApplicationContext().getPackageName() + "/files/" + params);
        FileInputStream inStream = new FileInputStream(absolutePath);


        ObjectInputStream objectInStream = new ObjectInputStream(inStream);
        int count = objectInStream.readInt(); // Get the number of regions
        List<Recipe> listOfStoragedRecipe = new ArrayList<Recipe>();
        for (int c = 0; c < count; c++)
            listOfStoragedRecipe.add((Recipe) objectInStream.readObject());
        objectInStream.close();

        //Log.d(TAG, "=============== LISTA FINALLL ==> " + rl.toString());
        return listOfStoragedRecipe;
    }


    /* (non-Javadoc)
  * @see android.app.Activity#onDestroy()
  */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "On Destroy .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onPause()
    */
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "On Pause .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onRestart()
    */
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "On Restart .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onResume()
    */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "On Resume .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onStart()
    */
    @Override
    protected void onStart() {
        super.onStart();

        Log.i(TAG, "On Start .....");
        HelperUtils.checkIsServerAlive();
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onStop()
    */
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "On Stop .....");
    }


    private void showListRecipes() {

        File cacheDir = getBaseContext().getCacheDir();
        //Create a File for your desired directory
        currentRecipesDirFolder = new File(cacheDir, RECIPES_CACHE_FOLDER + "_"+ HelperUtils.getUserDataFirebase().getUserId());

        //Call mkdirs() on that File to create the directory if it does not exist
        //Call mkdirs() on that File to create the directory if it does not exist
        if (!currentRecipesDirFolder.exists()) {
            //directory is created;
            currentRecipesDirFolder.mkdir();
        }

        //currentRecipesDirFolder = new File("/data/data/" + getContext().getPackageName() + "/files");
        int numOfStorageRecipes = currentRecipesDirFolder.listFiles().length;

        if (numOfStorageRecipes == 0) {
            TextView emptyList = (TextView) findViewById(R.id.activityTitle1);
            emptyList.setText("Não existem listas geradas");
        } else {
            //Para saber se recebe a lista de receitas proveniente da selecao manual da memoria
            if (getIntent().getExtras() != null) {
                fileSelected = (File) getIntent().getSerializableExtra("storagedFileSelected");
                try {
                    recipeList = readRecipesFromInternalMemory(fileSelected.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            } else {


                files = currentRecipesDirFolder.listFiles();


                Arrays.sort(files, new Comparator<File>() {
                    public int compare(File f1, File f2) {
                        return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
                    }
                });

                try {
                    fileSelected = files[files.length - 1];
                    recipeList = readRecipesFromInternalMemory(fileSelected.getAbsolutePath());


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            }

            if (recipeList != null && !recipeList.isEmpty()) {
                recipeNames = new ArrayList<String>();

                for (Recipe recipe : recipeList) {
                    recipeNames.add(recipe.getNameRecipe());
                }


                ArrayAdapter adapter = new ArrayAdapter(ActivityTwo.this, android.R.layout.simple_list_item_1, recipeNames);
                listView.setAdapter(adapter);

                SwipeMenuCreator creator = getSwipeMenuCreator();

                listView.setMenuCreator(creator);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Recipe recipeSelected = recipeList.get(position);
                        startActivtyRecipeDetail(recipeSelected);

                    }
                });


                listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                        switch (index) {
                            case 0:
                                Log.d(TAG, "onMenuItemClick: clicked Recipe go to Detail " + index);
                                Recipe recipeSelected = recipeList.get(position);
                                startActivtyRecipeDetail(recipeSelected);
                                break;
                            case 1:
                                //alert dialog to delete recipe and generate new one
                                createAlertDialog(position);

                                break;
                        }
                        // false : close the menu; true : not close the menu
                        return false;
                    }
                });
            }
        }
    }

    private void createAlertDialog(final int position) {


        AlertDialog alert = new AlertDialog.Builder(ActivityTwo.this).create();
        alert.setTitle("Warning");
        alert.setMessage("Do you want to delete this recipe?");
        alert.setButton(Dialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                recipeList.remove(position);
                recipeNames.remove(position);

                if (recipeList.size() != 0) {
                    try {
                        writeRecipesFromInternalMemory(recipeList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                fileSelected.delete();
                //finish();
                ArrayAdapter adapter = new ArrayAdapter(ActivityTwo.this, android.R.layout.simple_list_item_1, recipeNames);
                listView.setAdapter(adapter);
            }
        });
        alert.setButton(Dialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                //finish();
            }
        });
        alert.setButton(Dialog.BUTTON_NEUTRAL, "Delete and generate new recipe", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                List<Integer> idListCategory = new ArrayList<Integer>();
                idListCategory.add(1);
                idListCategory.add(2);

                RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
                Call<List<Recipe>> call = taskService.getRecipesByNumOfIng(idListCategory, 1);
                call.enqueue(new Callback<List<Recipe>>() {
                    @Override
                    public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                        List<Recipe> newRecipeGenerated = response.body();

                        if (newRecipeGenerated != null && !newRecipeGenerated.isEmpty()) {


                            recipeList.remove(position);
                            recipeNames.remove(position);

                            recipeList.add(newRecipeGenerated.get(0));
                            recipeNames.add(newRecipeGenerated.get(0).getNameRecipe());

                            if (recipeList.size() != 0) {
                                try {
                                    writeRecipesFromInternalMemory(recipeList);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            fileSelected.delete();
                            //finish();
                            ArrayAdapter adapter = new ArrayAdapter(ActivityTwo.this, android.R.layout.simple_list_item_1, recipeNames);
                            listView.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Recipe>> call, Throwable t) {

                    }
                });
                //finish();
            }
        });

        alert.show();


    }

    private SwipeMenuCreator getSwipeMenuCreator() {

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0x00, 0x66,
                        0xff)));
                // set item width
                openItem.setWidth(170);
                // set item title
                openItem.setTitle("Open");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(170);
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        return creator;

    }


}





