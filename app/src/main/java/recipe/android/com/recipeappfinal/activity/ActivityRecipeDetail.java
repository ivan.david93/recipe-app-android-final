package recipe.android.com.recipeappfinal.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.adapter.SwipeAdapter;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.IngredientHelper;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.model.UserDataFirebase;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 14/08/2017.
 */

public class ActivityRecipeDetail extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ActivityRecipeDetail";
    SwipeAdapter swipeAdapter;

    ViewPager viewPager;

    private RatingBar ratingBar;
    private Recipe recipe;

    private TextView textVRecipeName;
    private TextView textVRate;
    private TextView textVDiffic;
    //private TextView textViewIngredients;
    private TextView textViewSteps;

    private List<IngredientHelper> recipeIngredients;

    private Button btnWrite;
    private Button btnRead;
    private CheckBox favouritBtn;

    private ProgressDialog progressDialog;
    private Button showIngrBtn;
    private ImageView backBtn;

    ListView listView;
    private TextView textViewIngredientDetail;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_recipe_detail);


        getRecipeFromIntent();

        //activity fields

        textVRecipeName = (TextView) findViewById(R.id.textViewRecipeName);
        textVRate = (TextView) findViewById(R.id.textViewRate);
        textVDiffic = (TextView) findViewById(R.id.textViewDiffic);
        textViewSteps = (TextView) findViewById(R.id.textViewSteps);
        showIngrBtn = (Button) findViewById(R.id.show_ingredient_list);
        //textViewIngredients = (TextView) findViewById(R.id.textViewIngredients);

        progressDialog = new ProgressDialog(this);

        favouritBtn = (CheckBox) findViewById(R.id.selector_like_heart);

        backBtn = (ImageView) findViewById(R.id.backBtn);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        favouritBtn.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               Log.d(TAG, "On favourite clicked");
                                               //favouritBtn.setEnabled(false);
                                               addRecipeToFav();
                                           }
                                       }
        );

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        swipeAdapter = new SwipeAdapter(this);
        viewPager.setAdapter(swipeAdapter);

        loadText();

        addListenerOnRatingBar();

        showIngrBtn.setOnClickListener(this);
        backBtn.setOnClickListener(this);

        //CHECK IF USER HAS EVALUATED RECIPE OR FAVOURIT RECIPE
        checkUserRecipeInfo();

    }

    private void checkUserRecipeInfo() {
        //CHECK IF RECIPE IS A FAVOURITE AND BUTTON CHECK
        List<String> favRecipes = HelperUtils.getUserDataFirebase().getFavouritRecipes();

        if (favRecipes != null && !favRecipes.isEmpty()) {
            ArrayList<String> auxfavRecipes = new ArrayList<String>(favRecipes);
            // if favourti list has value of id recipe  button checked
            if (auxfavRecipes.contains(Integer.toString(recipe.getIdRecipe()))) {
                favouritBtn.setChecked(true);

            }
        }
        //CHECK RECIPE IS EVALUTAED AND DISABLE EVAL 
        HashMap<String, String> evalRecipes = HelperUtils.getUserDataFirebase().getRecipesEvalId();

        if (evalRecipes != null && !evalRecipes.isEmpty()) {
            HashMap<String, String> auxEvalRecipes = new HashMap<>(evalRecipes);
            //if eval listhas values recipe already evaluated disable rating bar
            if (auxEvalRecipes.get(Integer.toString(recipe.getIdRecipe())) != null) {
                ratingBar.setEnabled(false);
            }

        }
    }

    private void addRecipeToFav() {


        if (recipe != null) {

            //TODO check is already in favourit list  by Id Recipe
            List<String> recipesFavourit = HelperUtils.getUserDataFirebase().getFavouritRecipes();

            if (recipesFavourit == null) {
                return;
            }

            progressDialog.setMessage("Saving user info...");
            progressDialog.show();

            if (recipesFavourit.contains(Integer.toString(recipe.getIdRecipe()))) {
                //recipe already in favs list
                recipesFavourit.remove(Integer.toString(recipe.getIdRecipe()));
                toastMessage("Recipe removed from favourite!");
            } else {

                //add recipe to favouritList on firebase
                recipesFavourit.add(Integer.toString(recipe.getIdRecipe()));
                toastMessage("Recipe added to favourite!");
            }

            //save user to firebase with update user
            DatabaseReference databaseReferenceUsers = HelperUtils.getDatabaseReferenceUsers();
            UserDataFirebase userDataFirebase = HelperUtils.getUserDataFirebase();
            FirebaseUser firebaseUser = HelperUtils.getFirebaseUser();

            userDataFirebase.setFavouritRecipes(recipesFavourit);
            databaseReferenceUsers.child(firebaseUser.getUid()).setValue(userDataFirebase);

            toastMessage("Information Saved to Firebase...");
            progressDialog.dismiss();
        }

    }

    private void addListenerOnRatingBar() {


        //if rating value is changed,
        //display the current rating value in the result (textview) automatically
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                rateRecipe(rating, recipe.getIdRecipe());

                //toastMessage(String.valueOf(rating));


            }
        });
    }

    private void rateRecipe(final float rating, Integer idRecipe) {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<Recipe> call = taskService.rateRecipe(rating, idRecipe);
        call.enqueue(new Callback<Recipe>() {
            @Override
            public void onResponse(Call<Recipe> call, Response<Recipe> response) {

                recipe = response.body();

                if (recipe != null) {

                    updateUserFirebase(rating);
                    ratingBar.setEnabled(false);
                    setFields(recipe);
                    Log.d(TAG, "onResponse Recipe  ----------> " + recipe);
                }
            }

            @Override
            public void onFailure(Call<Recipe> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(TAG, "Error rateRecipe call");
            }
        });
    }

    private void updateUserFirebase(float rating) {

        if (recipe != null) {

            //TODO check is already in favourit list  by Id Recipe
            HashMap<String, String> recipesEval = HelperUtils.getUserDataFirebase().getRecipesEvalId();

            progressDialog.setMessage("Saving user info...");
            progressDialog.show();
            if (recipesEval == null) {
                recipesEval = new HashMap<String, String>();
            }
            recipesEval.put(Integer.toString(recipe.getIdRecipe()), Float.toString(rating));

            //save user to firebase with update user
            DatabaseReference databaseReferenceUsers = HelperUtils.getDatabaseReferenceUsers();
            UserDataFirebase userDataFirebase = HelperUtils.getUserDataFirebase();
            FirebaseUser firebaseUser = HelperUtils.getFirebaseUser();

            userDataFirebase.setRecipesEvalId(recipesEval);
            databaseReferenceUsers.child(firebaseUser.getUid()).setValue(userDataFirebase);

            toastMessage("Information Saved to Firebase...");
            progressDialog.dismiss();
        }
    }

    private void getRecipeFromIntent() {

        //TODO ATRAVES DA INTENT IR BUSCAR UM OBJECTO RECIPE
        // To retrieve object in activity

        if (getIntent().getExtras() != null) {
            recipe = (Recipe) getIntent().getSerializableExtra("recipeObj");

            if (recipe != null) {
                getIngredientsFromRecipe(recipe.getIdRecipe());
            }

        }

        // PARA TESTE IR A BASE DE DADOS E BUSCAR UMA RECIPE
        //getRecipeFromDB();


    }

    private void getRecipeFromDB() {

        final int recipeId = 1;

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<Recipe> call = taskService.getRecipeById(recipeId);
        call.enqueue(new Callback<Recipe>() {
            @Override
            public void onResponse(Call<Recipe> call, Response<Recipe> response) {
                recipe = response.body();


                if (recipe != null) {
                    Log.d(TAG, "onResponse Recipe  ----------> " + recipe);

                    getIngredientsFromRecipe(recipeId);

                }

            }

            @Override
            public void onFailure(Call<Recipe> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(TAG, "Error getRecipeById");
            }
        });

    }

    private void getIngredientsFromRecipe(int idRecipe) {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<IngredientHelper>> call = taskService.getIngredientsFromRecipe(idRecipe);
        call.enqueue(new Callback<List<IngredientHelper>>() {

            @Override
            public void onResponse(Call<List<IngredientHelper>> call, Response<List<IngredientHelper>> response) {

                recipeIngredients = response.body();

                if (recipeIngredients != null && !recipeIngredients.isEmpty()) {
                    Log.d(TAG, "onResponse ingredientList  ----------> " + recipeIngredients);

                    setFields(recipe);
                }
            }

            @Override
            public void onFailure(Call<List<IngredientHelper>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(TAG, "Error getIngredientsFromRecipe");
            }
        });
    }

    private void setFields(Recipe recipe) {

        String ingr = "";

        for (IngredientHelper ingredienthelper : recipeIngredients) {
            String name = ingredienthelper.getIngredientName();
            int quantity = ingredienthelper.getQuantity();
            String measure = ingredienthelper.getMeasure().getInitialsMeasure();

            ingr += "- " + quantity + " " + measure + " of " + name + "\n";

        }


        //textViewIngredients.setText(ingr);

        textVRecipeName.setText(recipe.getNameRecipe());

        textVRate.setText("Rate : " + Integer.toString(recipe.getRating()));
        textVDiffic.setText("Diffic : " + Integer.toString(recipe.getDifficulty()));
        textViewSteps.setText(recipe.getDirections());

    }

    private void loadText() {

        textViewSteps.setMovementMethod(new ScrollingMovementMethod());
        textViewSteps.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
                "Cras dapibus neque maximus justo fringilla, non vehicula metus posuere.\n" +
                "Maecenas volutpat dolor a elit mattis pulvinar.\n" +
                "Praesent ut quam vel sem vulputate sagittis.\n" +
                "Donec non orci sagittis, varius magna et, dignissim erat.\n" +
                "Ut sit amet libero sit amet lorem pulvinar pellentesque.\n" +
                "Maecenas tincidunt elit eget porta gravida.\n" +
                "Sed et mi venenatis, posuere nunc sed, malesuada neque.\n" +
                "Suspendisse hendrerit nulla at libero finibus suscipit.\n" +
                "Donec aliquam eros eu est dapibus venenatis.\n" +
                "Etiam in nunc ornare, iaculis quam et, fermentum nisi.");
    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.backBtn:
                finish();

                break;
            case R.id.show_ingredient_list:
                showIngredientsDialog(view);

                break;
        }
    }


    private void showIngredientsDialog(View view) {

        List<String> item = new ArrayList<>();

        for (IngredientHelper ingredienthelper : recipeIngredients) {
            String name = ingredienthelper.getIngredientName();
            int quantity = ingredienthelper.getQuantity();
            String measure = ingredienthelper.getMeasure().getInitialsMeasure();

            String ingr = "- " + quantity + " " + measure + " of " + name + "\n";
            item.add(ingr);

        }

        listView = new ListView(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_ingredients_detail, R.id.txtItem, item);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ViewGroup viewGroup = (ViewGroup) view;
                textViewIngredientDetail = (TextView) viewGroup.findViewById(R.id.txtItem);
            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityRecipeDetail.this);
        builder.setCancelable(true);
        builder.setPositiveButton("OK", null);
        builder.setTitle("Ingredients List");
        builder.setView(listView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

   /* public void writeMessage(View view) {

        String message = "Some message";
        String TMP_FILE_NAME = "tmp_file.txt";

       *//* try {
            FileOutputStream fileOutputStream = openFileOutput(file_name, MODE_PRIVATE);

            FileOutputStream fileOutputStream3 = openFileOutput(

            fileOutputStream.write(message.getBytes());
            fileOutputStream.close();
            toastMessage("Message saved");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file = getFileStreamPath(file_name);
        File dir = getFilesDir();*//*

        File cacheDir = getBaseContext().getCacheDir();
        //Create a File for your desired directory
        File direct=new File(cacheDir,"testFolder");
        //Call mkdirs() on that File to create the directory if it does not exist
        if(!direct.exists()) {
            if(direct.mkdir()); //directory is created;
        }
        //Create a File for the output file
        File file=new File(direct,TMP_FILE_NAME);


        FileOutputStream outstream;

        try{
            if(!file.exists()){
                file.createNewFile();
            }

            //commented line throws an exception if filename contains a path separator
            //outstream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outstream = new FileOutputStream(file);
            outstream.write(message.getBytes());
            outstream.close();

        }catch(IOException e){
            e.printStackTrace();
        }


    }*/






/*

    public void readMessage(View view) {

       */
/* try {
            String message;
            FileInputStream fileInputStream = openFileInput("testFile");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            while ((message = bufferedReader.readLine()) != null) {
                stringBuffer.append(message + "\n");
                Log.d(TAG,message);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*//*



        String TMP_FILE_NAME = "tmp_file.txt";
        File tmpFile;

        File cacheDir = getBaseContext().getCacheDir();
        tmpFile = new File(cacheDir.getPath() + "/testFolder/" + TMP_FILE_NAME) ;

        String line="";
        StringBuilder text = new StringBuilder();

        try {
            FileReader fReader = new FileReader(tmpFile);
            BufferedReader bReader = new BufferedReader(fReader);

            while( (line=bReader.readLine()) != null  ){
                text.append(line+"\n");
            }
            Log.d(TAG,text.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

    }
*/

}
