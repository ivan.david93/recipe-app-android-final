package recipe.android.com.recipeappfinal.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.adapter.SectionsPagerAdapter;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.fragments.TabByIngredientsFragment;
import recipe.android.com.recipeappfinal.fragments.TabByNrOfIngrFragment;
import recipe.android.com.recipeappfinal.model.CategoryA;
import recipe.android.com.recipeappfinal.model.CategoryB;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 05/08/2017.
 */

//public class ActivityThree extends AppCompatActivity {
public class ActivityThree extends BaseActivity {


    private static final String TAG = "ActivityThree";

    private SectionsPagerAdapter sectionsPagerAdapter;

    private ViewPager viewPager;
    private List<CategoryA> lisOfAllCategoriesA;
    private List<CategoryB> lisOfAllCategoriesB;

    DataFromActivityToFragment dataFromActivityToFragment;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_three, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);

        //setContentView(R.layout.activity_three);

//        TextView title3 = (TextView) findViewById(R.id.activityTitle3);
//        title3.setText("This is ACTIVITY GENERATE MEALS");

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.container);

        setupViewPager(viewPager);

        getAllCategories();

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_filter_5_black);
//        tabLayout.getTabAt(1).setIcon(R.drawable.ic_shopping_cart_black);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_kitchen_black);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        //TODO DISABLE SHIFT MODE ON BOTTOM IF WANTED HERE
        // BottomNavViewHelper.disableShiftMode(bottomNavigationView);
        // declarare items menu to highlighht each button
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_home_black:
                        Intent intent1 = new Intent(ActivityThree.this,MainActivity.class);
                        startActivity(intent1);
                        finish();
                        break;
                    case R.id.ic_view_list:
                        Intent intent2 = new Intent(ActivityThree.this,ActivityTwo.class);
                        startActivity(intent2);
                        finish();
                        break;
//                    case R.id.ic_shuffle_black:
//                        Intent intent3 = new Intent(ActivityThree.this,ActivityThree.class);
//                        startActivity(intent3);
//                        break;
                }
                return false;

            }
        });


    }

    public interface DataFromActivityToFragment {
        void sendData(String data);

        void sendDataList(List<CategoryB> lisOfAllCategoriesB);
    }


    public void setupViewPager(ViewPager upViewPager) {
        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // do this to pass parameters to the fragment
//        Bundle bundle = new Bundle();
//        bundle.putSerializable("lisOfAllCategoriesB", (Serializable) lisOfAllCategoriesB);
        TabByIngredientsFragment tabByIngredientsFragment = new TabByIngredientsFragment();
//        tabByIngredientsFragment.setArguments(bundle);
        dataFromActivityToFragment = (DataFromActivityToFragment) tabByIngredientsFragment;

        adapter.addFragment(new TabByNrOfIngrFragment());
        adapter.addFragment(tabByIngredientsFragment);

        viewPager.setAdapter(adapter);
    }


    private void getAllCategories() {

        getCategoriesA();
        getCategoriesB();

    }


    private void getCategoriesA() {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<CategoryA>> call = taskService.getCategoriesA();
        call.enqueue(new Callback<List<CategoryA>>() {
            @Override
            public void onResponse(Call<List<CategoryA>> call, Response<List<CategoryA>> response) {


                lisOfAllCategoriesA = response.body();

                if (lisOfAllCategoriesA != null && !lisOfAllCategoriesA.isEmpty()) {
                    for (int i = 0; i < lisOfAllCategoriesA.size(); i++) {
                        Log.d(TAG, "onResponse : " + lisOfAllCategoriesA.get(i));
                    }

                }

            }

            @Override
            public void onFailure(Call<List<CategoryA>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(TAG,"Error getCategoriesA");
            }
        });

    }

    private void getCategoriesB() {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<CategoryB>> call = taskService.getCategoriesB();
        call.enqueue(new Callback<List<CategoryB>>() {
            @Override
            public void onResponse(Call<List<CategoryB>> call, Response<List<CategoryB>> response) {

                lisOfAllCategoriesB = response.body();

                if (lisOfAllCategoriesB != null && !lisOfAllCategoriesB.isEmpty()) {
                    dataFromActivityToFragment.sendDataList(lisOfAllCategoriesB);
                    for (int i = 0; i < lisOfAllCategoriesB.size(); i++) {
                        Log.d(TAG, "onResponse : " + lisOfAllCategoriesB.get(i));
                    }
                }
            }

            @Override
            public void onFailure(Call<List<CategoryB>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(TAG,"Error getCategoriesB");
            }
        });
    }
}
