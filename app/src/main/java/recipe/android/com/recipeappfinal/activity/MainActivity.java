package recipe.android.com.recipeappfinal.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.adapter.SectionsPagerAdapter;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.fragments.Tab1Fragment;
import recipe.android.com.recipeappfinal.fragments.Tab2Fragment;
import recipe.android.com.recipeappfinal.fragments.Tab3Fragment;
import recipe.android.com.recipeappfinal.model.User;
import recipe.android.com.recipeappfinal.model.UserDataFirebase;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//public class MainActivity extends AppCompatActivity {
public class MainActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";





    private SectionsPagerAdapter sectionsPagerAdapter;

    private ViewPager viewPager;

    //add Firabase Database Stuff
//    private FirebaseDatabase firebaseDatabase; // TODO
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
//    private DatabaseReference myRef; // TODO

    private FirebaseAuth.AuthStateListener mAuthListener;

    private ProgressDialog progressDialog;


    //after login
    private Button btnTestDialog;
    String[] listDiets;
    boolean[] checkedItems;
    ArrayList<Integer> mDietTypes = new ArrayList<>();
    private String newUserId;
    private List<Integer> userCategoryList;

    FirebaseUser firebaseUser;

    private DatabaseReference databaseReferenceUsers;

    private UserDataFirebase userDataFirebase;

    private TextView textViewUserNameProfL;

    //BUTTONS HOME SCREEN
    private ImageButton imgBtnGenrtMeals;
    private ImageButton imgBtnListOfMeals;
    private ImageButton imgBtnMealsStoraged;

    private ImageButton imgBtnFavourite;
    private ImageButton imgBtnAccStettings;
    private ImageButton imgBtnLogOut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_main, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);


        imgBtnGenrtMeals = (ImageButton) findViewById(R.id.imgBtnGenerateMeals);
        imgBtnListOfMeals = (ImageButton) findViewById(R.id.imgBtnListOfMeals);
        imgBtnMealsStoraged = (ImageButton) findViewById(R.id.imgBtnMealsStoraged);
        imgBtnFavourite = (ImageButton) findViewById(R.id.imgBtnFavourite);
        imgBtnAccStettings = (ImageButton) findViewById(R.id.imgBtnAccStettings);
        imgBtnLogOut = (ImageButton) findViewById(R.id.imgBtnLogOut);
        
        //setContentView(R.layout.activity_main);

        progressDialog = new ProgressDialog(this);

        databaseReferenceUsers = FirebaseDatabase.getInstance().getReference("users");

        //initializing firebase authentication object
        firebaseAuth = FirebaseAuth.getInstance();

        //if the user is not logged in that means current uiser will return null
        firebaseUser = firebaseAuth.getCurrentUser();

        if (firebaseUser == null) {

            //start loginactivty
            startActivity(new Intent(this, LoginActivity.class));
            //close this activity
            finish();

        } else {
            //THIS IS A HELPER CLASS THAT GET DATA FROM FIREBASE AND SAVE USER TO BE USED AT EVERY CLASS AND CONTEXT
            //TODO ONLY OPEN APLUICATION IF EVERYTHING GOES OK
            HelperUtils.getCurrentUserDataFromFireBase(firebaseUser, this);
        }
/*
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        viewPager = (ViewPager) findViewById(R.id.container);

        //setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_info_outline_black);
//        tabLayout.getTabAt(1).setIcon(R.drawable.ic_shopping_cart_black);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_info_outline_black);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        //TODO DISABLE SHIFT MODE ON BOTTOM IF WANTED HERE
        // BottomNavViewHelper.disableShiftMode(bottomNavigationView);

        // declarare items menu to highlighht each button
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
//                    case R.id.ic_home_black:
//                        Intent intent1 = new Intent(MainActivity.this,ActivityOne.class);
//                        startActivity(intent1);
//                        break;
                    case R.id.ic_view_list:
                        Intent intent2 = new Intent(MainActivity.this, ActivityTwo.class);
                        startActivity(intent2);
                        finish();
                        break;
                    case R.id.ic_shuffle_black:
                        //dont go to activity 3 generate if server not alive
                        if (HelperUtils.isAlive()) {
                            Intent intent3 = new Intent(MainActivity.this, ActivityThree.class);
                            startActivity(intent3);
                            finish();
                        } else {
                            HelperUtils.serverDownMessage(MainActivity.this, "Can´t navigate to Generate Recipe");
                        }

                        break;

                }

                return false;

            }


        });
*/

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                    toastMessage("Successfully signed in with: " + user.getEmail());


                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    //toastMessage("Successfully signed out");
                }
                // ...
            }


        };

        userCategoryList = new ArrayList<Integer>();


        // if receive something from register activity
        if (getIntent().getExtras() != null) {
            userDataFirebase = (UserDataFirebase) getIntent().getSerializableExtra("userDataFirebase");

            if (userDataFirebase != null) {
                //this is for the dialog box
                listDiets = getResources().getStringArray(R.array.diet_types);
                checkedItems = new boolean[listDiets.length];

                runDietTypeDialog();
            }

        }

        //BUTTONS SCREEN  HOME LISTENER
        imgBtnGenrtMeals.setOnClickListener(this);
        imgBtnListOfMeals.setOnClickListener(this);
        imgBtnMealsStoraged.setOnClickListener(this);

        imgBtnFavourite.setOnClickListener(this);
        imgBtnAccStettings.setOnClickListener(this);
        imgBtnLogOut.setOnClickListener(this);

    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void setupViewPager(ViewPager upViewPager) {
//        SectionsPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());

        sectionsPagerAdapter.addFragment(new Tab1Fragment());
        sectionsPagerAdapter.addFragment(new Tab2Fragment());
        sectionsPagerAdapter.addFragment(new Tab3Fragment());
        viewPager.setAdapter(sectionsPagerAdapter);
    }


    private void runDietTypeDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.str_diet_types);
        builder.setMultiChoiceItems(listDiets, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position, boolean isChecked) {

                if (isChecked) {
                    //verify if item is selected or not
                    if (!mDietTypes.contains(position)) {
                        mDietTypes.add(position);
                    }

                } else if (mDietTypes.contains(position)) {
                    // removes the object X - if pass only position  thinks that "position" is a index not a object
                    mDietTypes.remove(Integer.valueOf(position));
                }
            }
        });
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.str_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //TODO IF Ok guarda na database categorias de user diet type para este user
                for (int i = 0; i < mDietTypes.size(); i++) {
                    Log.d(TAG, "mDietTypes: " + listDiets[mDietTypes.get(i)]);
                    // adiciona os ids das categorias pretendidas pelo utilizador à lista
                    userCategoryList.add(mDietTypes.get(i) + 1);
                }
                toastMessage("onClick ok dialog");
                saveUserToDatabase();

            }
        });
//        builder.setNegativeButton(R.string.str_dismiss, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });

        builder.setNeutralButton(R.string.str_clear_all, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                for (int i = 0; i < checkedItems.length; i++) {
                    checkedItems[i] = false;
                    mDietTypes.clear();

                }
            }
        });

        AlertDialog mdialog = builder.create();

        mdialog.show();
    }


    private void saveUserToDatabase() {

        progressDialog.setMessage("Saving user info...");
        progressDialog.show();
        // CRIAR UTILIZADR NA BD DO SERVIDOR E ASSOCIAR CATEGORIAS DE UTILIZADOR
        addUsertoBD(userCategoryList);
        //getUsersTest();
    }


    private void addUsertoBD(List<Integer> userCategoryList) {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<User> call = taskService.addUsertoBD(userCategoryList);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                User newUserFromDB = response.body();
                if (newUserFromDB != null) {
                    Log.d(TAG, "onResponse newUserFromDB : " + newUserFromDB.getIdUser());

                    addUsertoFirebaseDB(newUserFromDB.getIdUser());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                // something went completely south (like no internet connection)
                progressDialog.dismiss();
                Log.d(TAG, "Error addUsertoBD");
            }
        });
    }

    private void addUsertoFirebaseDB(Integer idUser) {

        userDataFirebase.setUserId(Integer.toString(idUser));

        databaseReferenceUsers.child(firebaseUser.getUid()).setValue(userDataFirebase);


        toastMessage("Information Saved to Firebase...");

        progressDialog.dismiss();

    }

    /* (non-Javadoc)
    * @see android.app.Activity#onDestroy()
    */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "On Destroy .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onPause()
    */
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "On Pause .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onRestart()
    */
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "On Restart .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onResume()
    */
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "On Resume .....");
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onStart()
    */
    @Override
    protected void onStart() {
        super.onStart();

        Log.i(TAG, "On Start .....");
        HelperUtils.checkIsServerAlive();
    }

    /* (non-Javadoc)
    * @see android.app.Activity#onStop()
    */
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "On Stop .....");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgBtnGenerateMeals:
                goToGenerateMeals();

                break;

            case R.id.imgBtnListOfMeals:
                goToListMeals();

                break;

            case R.id.imgBtnMealsStoraged:
                goToStoraged();

                break;

            case R.id.imgBtnFavourite:
                goToFavourites();
                break;

            case R.id.imgBtnAccStettings:
                goToSettings();

                break;

            case R.id.imgBtnLogOut:
                goToLogin();
                break;

        }
    }

    private void goToFavourites() {

        //dont go to activity 3 generate if server not alive
        if (HelperUtils.isAlive()) {
            Intent intentF = new Intent(MainActivity.this, ActivityShowFavourits.class);
            startActivity(intentF);
            finish();
        } else {
            HelperUtils.serverDownMessage(MainActivity.this, "Can´t navigate to Favourites");
        }

    }

    private void goToGenerateMeals() {

        //dont go to activity 3 generate if server not alive
        if (HelperUtils.isAlive()) {
            Intent intent1 = new Intent(MainActivity.this, ActivityThree.class);
            startActivity(intent1);
            finish();
        } else {
            HelperUtils.serverDownMessage(MainActivity.this, "Can´t navigate to Generate Recipe");
        }

    }

    private void goToListMeals() {

        Intent intent2 = new Intent(MainActivity.this, ActivityTwo.class);
        startActivity(intent2);
        finish();

    }

    private void goToStoraged() {

        Intent intent3 = new Intent(MainActivity.this, ActivityListStoragedRecipes.class);
        startActivity(intent3);
        finish();
    }

    private void goToSettings() {
        //toastMessage("nav_account_settings...");
        //dont go to activity 3 generate if server not alive
        if (HelperUtils.isAlive()) {
            Intent storageAccSettingsIntent = new Intent(MainActivity.this, ProfileScreenXMLUIDesign.class);
            startActivity(storageAccSettingsIntent);
            finish();
        } else {
            HelperUtils.serverDownMessage(MainActivity.this, "Can´t navigate to account settings");
        }

    }

    private void goToLogin() {
        toastMessage("Signing Out...");
        firebaseAuth.signOut();

        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
        finish();
    }


}
