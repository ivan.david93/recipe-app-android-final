package recipe.android.com.recipeappfinal.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.activity.ActivityRecipeDetail;
import recipe.android.com.recipeappfinal.activity.ActivityTwo;
import recipe.android.com.recipeappfinal.activity.MainActivity;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IVAN on 05/08/2017.
 */

public class Tab3Fragment extends Fragment {

    private static final String TAG = "Tab3Fragment";

    private Button btnTEST;
    private Button btnRecipesTest;

    List<Recipe> lisOfAlltRecipes;
    private Button btnTestDetail;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment3_layout, container, false);


        btnTEST = (Button) view.findViewById(R.id.btnTest);
        btnRecipesTest = (Button) view.findViewById(R.id.btnRecipesTest);
        btnTestDetail = (Button) view.findViewById(R.id.btnTestDetail);

        lisOfAlltRecipes = null;



        btnTEST.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Toast.makeText(getActivity(), "TESTING BUTTON CLICK 3 ", Toast.LENGTH_LONG).show();
            }
        });


        btnRecipesTest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //teste obter as receitas
                getRecipes();

            }
        });

        btnTestDetail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               //teste activity detail
                Intent intent2 = new Intent(getContext(),ActivityRecipeDetail.class);
                startActivity(intent2);

            }
        });



        return view;


    }

    public void getRecipes() {

        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<Recipe>> call = taskService.getRecipes();
        call.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {


                lisOfAlltRecipes = response.body();

                if (!lisOfAlltRecipes.isEmpty()) {
                    for (int i = 0; i < lisOfAlltRecipes.size(); i++) {
                        Log.d(TAG, "onResponse : " + lisOfAlltRecipes.get(i).getNameRecipe());
                    }

                }

            }

            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
            }
        });
    }


}
