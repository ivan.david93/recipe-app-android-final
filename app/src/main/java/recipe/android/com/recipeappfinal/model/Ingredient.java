package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ingredient implements Serializable {

    @SerializedName("idIngredient")
    @Expose
   private Integer idIngredient;

    @SerializedName("ingredientName")
    @Expose
    private String ingredientName;

    @SerializedName("categoryIngredientB")
    @Expose
   private CategoryB categoryIngredientB;

    @SerializedName("measure")
    @Expose
    private Measure measure;

    @SerializedName("recipeList")
    @Expose
    private List<RecipeIngredient> recipeList = new ArrayList<RecipeIngredient>();

    public Ingredient() {
    }

    public Integer getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(Integer idIngredient) {
        this.idIngredient = idIngredient;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public CategoryB getCategoryIngredientB() {
        return categoryIngredientB;
    }

    public void setCategoryIngredientB(CategoryB categoryIngredientB) {
        this.categoryIngredientB = categoryIngredientB;
    }

    public Measure getMeasure() {
        return measure;
    }

    public void setMeasure(Measure measure) {
        this.measure = measure;
    }

    public List<RecipeIngredient> getRecipeList() {
        return recipeList;
    }

    public void setRecipeList(List<RecipeIngredient> recipeList) {
        this.recipeList = recipeList;
    }
}
