package recipe.android.com.recipeappfinal.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by pedro on 03/06/2017.
 */

public class RecipeIngredient implements Serializable {

    @SerializedName("recipe")
    @Expose
    private Recipe recipe;

    @SerializedName("ingredient")
    @Expose
    private Ingredient ingredient;

    @SerializedName("quantity")
    @Expose
    private int quantity;

    public RecipeIngredient() {
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public Ingredient getIngredient() {
        return ingredient;
    }

    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
