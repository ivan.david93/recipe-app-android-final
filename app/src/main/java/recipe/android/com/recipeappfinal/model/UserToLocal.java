package recipe.android.com.recipeappfinal.model;

import java.io.Serializable;

public class UserToLocal implements Serializable {


        private String email;
        private String md5Password;

        public UserToLocal(String email, String md5Password) {

            this.email = email;
            this.md5Password = encodePassword(md5Password);
        }

        private String encodePassword(String md5Password) {
            //TODO encrypt password
            return md5Password;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMd5Password() {
            return md5Password;
        }

        public void setMd5Password(String md5Password) {
            this.md5Password = md5Password;
        }


    }