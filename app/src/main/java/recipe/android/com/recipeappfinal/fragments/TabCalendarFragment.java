package recipe.android.com.recipeappfinal.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import recipe.android.com.recipeappfinal.R;

/**
 * Created by IVAN on 05/08/2017.
 */

public class TabCalendarFragment extends Fragment {

    private static final String TAG = "Tab3Fragment";

    private Button btnTEST;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_calendar_layout,container,false);
        btnTEST = (Button) view.findViewById(R.id.btnTest);



        btnTEST.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                Toast.makeText(getActivity(),"TESTING BUTTON CLICK 3 ", Toast.LENGTH_LONG).show();
            }
        });

        return view;


    }


}
