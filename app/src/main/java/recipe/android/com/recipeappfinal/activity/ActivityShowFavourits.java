package recipe.android.com.recipeappfinal.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;
import java.util.List;

import recipe.android.com.recipeappfinal.R;
import recipe.android.com.recipeappfinal.api.RecipeAPI;
import recipe.android.com.recipeappfinal.model.Recipe;
import recipe.android.com.recipeappfinal.utils.HelperUtils;
import recipe.android.com.recipeappfinal.web.ServiceGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pedro on 26/08/2017.
 */

public class ActivityShowFavourits extends BaseActivity {

    private static final String TAG = "Activity Favourits";
    SwipeMenuListView listView;
    List<String> favouritesListString;
    List<Integer> favouritesListInt;
    private List<Recipe> lisOfFavouriteRecipes;
    private List<String> recipeNames;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        favouritesListString = new ArrayList<>();
        favouritesListInt = new ArrayList<>();
        recipeNames = new ArrayList<>();


        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_two, contentFrameLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        //TODO DISABLE SHIFT MODE ON BOTTOM IF WANTED HERE
        // BottomNavViewHelper.disableShiftMode(bottomNavigationView);

        // declarare items menu to highlighht each button
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        listView = (SwipeMenuListView) findViewById(R.id.listView);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.ic_home_black:
                        Intent intent1 = new Intent(ActivityShowFavourits.this, MainActivity.class);
                        startActivity(intent1);
                        finish();
                        break;
                    case R.id.ic_view_list:
                        Intent intent2 = new Intent(ActivityShowFavourits.this, ActivityTwo.class);
                        startActivity(intent2);
                        break;
                    case R.id.ic_shuffle_black:
                        //dont go to activity 3 generate if server not alive
                        if (HelperUtils.isAlive()) {
                            Intent intent3 = new Intent(ActivityShowFavourits.this, ActivityThree.class);
                            startActivity(intent3);
                            finish();
                        } else {
                            HelperUtils.serverDownMessage(ActivityShowFavourits.this, "Can´t navigate to Generate Recipe");
                        }
                        break;

                }
                return false;

            }
        });

        showListRecipes();

    }

    private void showListRecipes() {

        favouritesListString = HelperUtils.getUserDataFirebase().getFavouritRecipes();
        if (favouritesListString == null) {
            toastMessage("There is no favourites");
            finish();
        } else {
            //Converte List String para Int
            for(String s : favouritesListString) favouritesListInt.add(Integer.valueOf(s));
            getRecipeListByIds(favouritesListInt);
        }
        Log.d(TAG, "onResponse favouritesListString --------------> : " + favouritesListString.get(0));

    }

    private void toastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void getRecipeListByIds(List<Integer> favouritesListInt) {
        RecipeAPI taskService = ServiceGenerator.createService(RecipeAPI.class);
        Call<List<Recipe>> call = taskService.findListRecipesByIds(favouritesListInt);
        call.enqueue(new Callback<List<Recipe>>() {
            @Override
            public void onResponse(Call<List<Recipe>> call, Response<List<Recipe>> response) {
                lisOfFavouriteRecipes = new ArrayList<>();
                lisOfFavouriteRecipes = response.body();
                if (lisOfFavouriteRecipes != null) {
                    for (Recipe recipe : lisOfFavouriteRecipes) {
                        recipeNames.add(recipe.getNameRecipe());
                    }
                    createRecipeList(lisOfFavouriteRecipes,recipeNames);
                    //lisOfFavouriteRecipes = lisOfAllUserCategoriesResponse;

                }
                //Log.d(TAG, "As categorias existentes sao ---->" + lisOfAllUserCategories.get(0).getNameUserCategory());
            }

            @Override
            public void onFailure(Call<List<Recipe>> call, Throwable t) {
            }
        });
    }

    private SwipeMenuCreator getSwipeMenuCreator() {

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0x00, 0x66,
                        0xff)));
                // set item width
                openItem.setWidth(170);
                // set item title
                openItem.setTitle("Open");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
            }
        };

        return creator;

    }

    private void createRecipeList(final List<Recipe> lisOfFavouriteRecipes, List<String> recipeNames) {

        ArrayAdapter adapter = new ArrayAdapter(ActivityShowFavourits.this, android.R.layout.simple_list_item_1, recipeNames);
        listView.setAdapter(adapter);

        SwipeMenuCreator creator = getSwipeMenuCreator();

        listView.setMenuCreator(creator);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Recipe recipeSelected = lisOfFavouriteRecipes.get(position);
                startActivtyRecipeDetail(recipeSelected);

            }
        });


        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        Log.d(TAG, "onMenuItemClick: clicked Recipe go to Detail " + index);
                        Recipe recipeSelected = lisOfFavouriteRecipes.get(position);
                        startActivtyRecipeDetail(recipeSelected);
                        break;
                    case 1:
                        //alert dialog to delete recipe and generate new one
                        //createAlertDialog(position);

                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
    }

    private void startActivtyRecipeDetail(Recipe recipe) {

        Intent intent1 = new Intent(ActivityShowFavourits.this, ActivityRecipeDetail.class);

        intent1.putExtra("recipeObj", recipe);

        startActivity(intent1);
    }



}
